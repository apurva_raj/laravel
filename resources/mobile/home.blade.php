@extends('layout.main')

@section('metatags')
  <meta content="{{$title}}" property="og:title" />
  <meta content="{{$metaImg}}" property="og:image" />
  <title>{{$title}}</title>

@endsection
  @section('content')
  <div class="container">
  <!-- Position for Left Side Bar -->
    <div class="cd1">
      <!-- Position for Actual Content Area -->
      <div class = "post-feeds">
  			@include('post.post_ui')
  		</div>
    </div>
  </div>
@endsection
