@foreach($users as $user)
<li>
	<a href="{{ route('profile', $user->username) }}">
	<div class="search-logo">
	   		<img src="{{ url($user->profile->display_thumb_pic) }}" alt="" />
	 	<div class="search-name">
	        <span><strong>{{$user->name}}</strong></span>
		</div>
		<!--
		<div class="search-follow">
	        <span><strong>Follow</strong></span>
		</div>
	-->
	</div>
	</a>
</li>
@endforeach
