@include('layout.default')
<body>
  <section class= "main-container">
  		@include('layout.header')
  		<div class="content">
  			@yield('content')
  		</div>
  		<div class="home">
  			@yield('home')
  		</div>
  	</section>
    @yield('there')
	<div id="dataConfirmModal" class="modal">
	    <div class="modal-dialog">
	        <div class="modal-content">
	            <div class="modal-header">
	                <button type="button" class="btn close" data-dismiss="modal" aria-label="Close">
	                    <span aria-hidden="true">&times;</span>
	                </button>
		            <div class="modal-title">
		                <span id="lblTitleConfirmYesNo" class="modal-title"></span>
		            </div>
	            </div>
	            <div class="modal-body">
	                <p id="lblMsgConfirmYesNo"></p>
	            </div>
	            <div class="modal-footer">
	                <button id="btnYesConfirmYesNo"
	                type="button" class="btn confirm">Yes</button>
	                <button id="btnNoConfirmYesNo"
	                type="button" class="btn dismiss">No</button>
	            </div>
	        </div>
	    </div>
	</div>
</body>
