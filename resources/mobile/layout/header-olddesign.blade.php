<div class="topbar-container">
	@if (Auth::check())
	<!--Content Visible only to Autheticated Users -->
		<div class="navigation">
			<div class="navbar">
			<!--Website Logo Placeholder-->
					<div class="logo">
						<a href="{{ URL::route('home') }}"><span><img src="{{ asset('images/pictales-logo-by-hetu_archi.png') }}"></span></a>
					</div>

				<!--Search Bar Placeholder-->
				<div class="search">
					<form class="search-form" role="form" method="GET" action="{{ url('/search/user') }}">
						<div class="icon"><img src="{{ asset('images/search-icon01.png') }}"></div>
						<input id="search-query" type="text" name="query" placeholder="Search..."  autocomplete="off" />
					</form>
					<div class="search-dropdown">
						<ul>
		  				</ul>
					</div>
					<!--<button type="submit" class="btn" >Search</button>-->
				</div>
			</div>
			<div class="menubar">
				<ul>
					<li><a href="{{ URL::route('home') }}">Home</a></li>
					<li>Add Post</li>
					<li><a href="{{ route('notify.view') }}">Notifs</a></li>
					<li class="last"><a href=" {{ route('profile', Auth::user()->username) }}">Profile</a></li>
				</ul>
			</div>
		</div>
	@endif
	<!--Flash Placeholder -->
	<div class="flashholder">
	@include('layout.flash')
	</div>
</div>

<!-- <div class="navspacer">
</div> -->
