@extends('layout.main')

 @section('metatag')
    <title>{{ $title }}</title>
    <meta content="{{$title}}" property="og:title" />
    <meta content="{{$metaImg}}" property="og:image" />
  @endsection
  
@section('content')
  <div class="container">
    <div class="cd1">
  <ul>
  @if($notifications->count() > 0)
    @foreach($notifications as $notification)
      <li class="notifs">
        <a href=" {{ $notification['attributes']['url'] }} ">{{ $notification['attributes']['text'] }}</a>
      </li>
    @endforeach
  @else
    <li>
      <a>Nothing to show.</a>
    </li>
  @endif
</ul>
</div>
</div>
@endsection
