<div class="body" style="font-size:1.3em;">
<div class= "invite-bg" style="background-color:#eee; display:block; padding-bottom:20px;">
	<div class="center" style="display:block; margin:0 auto; padding-top: 20px; width:100px;">
		<img class= "logo" style="width:150px;" src="http://oi67.tinypic.com/15nwtoh.jpg" alt="Pictales" />
		<br>
	</div>
	<div class="content" style=" margin:20px 70px; min-width: 400px; padding:10px; background-color:#fff;">
	<h3>You are invited to Pictales</h3>

	<span><a href="http://www/pictales.me" style="text-decoration:none">Pictales.me</a></span>
	<div class="join-btn" style="background-color:#ddd; border-radius:5px; display:block; margin:20px auto; max-width:150px; text-align:center;">
		<a href="http://www.pictales.me/mailer/register?code={{$code}}" style="text-decoration:none; color:#000; display:block; padding:10px;">
			Join Pictales
		</a>
	</div>
	<div class="link" style="text-align:center; font-size:0.8em;">
		<span>You may copy/paste this link into your browser:</span>
		<br>

		<a href="http://www.pictales.me/mailer/register?code={{ $code }}">http://pictales.me/mailer/register?code={{ $code }}</a>
		<br><br>This Invite is sent to you by  {{ $user }}
	</div>
	<br>
	<div class="email" style="text-align:center;">
	<span>Your sign in email is:</span>
	<br> {{ $email }}
</div>
</div>
</div>
</div>
