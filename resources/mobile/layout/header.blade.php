@if (Auth::check())

<div class="topbar-container">
	<!--Content Visible only to Autheticated Users -->
		<div class="navigation">
			<div class="navbar">
			<!--Website Logo Placeholder-->

					<div class="explore">
						<a href="{{ route('explore') }}"><i class="material-icons" style="font-size:30px;color:#656464;">explore</i></a>
					</div>
					<div class="logo">
						<a href="{{ URL::route('home') }}"><span><img src="{{ asset('images/pictales-logo-by-hetu_archi.png') }}"></span></a>
					</div>


				<div class="mobile-option">
					<div class="">
						<span><i class="material-icons">more_horiz</i></span>
					</div>
				</div>
				<div class="dd" style="display:none;">
					<ul>
						<li><a href="{{ route('mailer.invite') }}">Invites</a></li>
						<li><a href="{{ route('about') }}">About</a></li>
						<li><a href="{{ url('auth/logout') }}">Logout</a></li>
					</ul>
					<div class="popover-arrow" style="margin-left: 100px;
    margin-top: -180px;"></div>
				</div>
				<!-- <div class="popover-arrow" style="margin-left: 8px;"></div> -->
			</div>
		</div>
	@endif
	<!--Flash Placeholder -->
	<div class="flashholder">
	@include('layout.flash')
	</div>
</div>

<div class="bottombar-container">
	@if (Auth::check())
	<!--Content Visible only to Autheticated Users -->
		<div class="navigation">
			<div class="menubar">
				<ul>
					<a href="{{ URL::route('home') }}"><li><div class="ah1"><i class="material-icons">home</i></div></li></a>
					<a href="{{ route('addpost') }}"><li><div class="ah1"><i class="material-icons">create</i></div></li></a>
					<a href="{{ route('notify.view') }}">
						<li>
							<div class="ah1">
								<i class="material-icons">notifications</i>

										@if($notify->count() > 0)
											<div class="notify-label">{{ $notify->count() }}</div>
										@elseif($notify->count() > 99)
											<div class="notify-label">99</div>
										@endif

							</div>
						</li>
					</a>
					<a href=" {{ route('profile', Auth::user()->username) }}"><li class="last"><div class="ah1"><i class="material-icons">account_circle</i></div></li></a>
				</ul>
			</div>
		</div>
	@endif
</div>


<!-- <div class="navspacer">
</div> -->
