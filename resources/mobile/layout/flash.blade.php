@if (Session::has('info'))
	<div class="flash centered">
		{{ Session::get('info') }}
	</div>
@endif
