
@extends('layout.main')
@section('metatag')

@endsection

@section('content')


<style media="screen">
	.top-right{
		margin-top: -52px !important;
	}
</style>
<div class="container" style="margin-top:35px;">
  <div class="main">
      <div class="tags_to_follow">
					<ul style="margin-left:-35px;">
						@foreach($topics as $topic)
						<a href="{{ route('topic_name', $topic->topic_slug_name) }}"><li class="tag tag_e">{{ $topic->topic_name }}</li></a>
						@endforeach
					</ul>
      </div>

			<div class="people_to_follow">

				<ul class="pf1">

					@foreach($users as $user)
					<div class="">

					<li class="people_e">
						<img src="{{ url($user->profile->display_pic) }}" alt="" class="img_e">
						<ul style="margin-left:-40px">

								<li class="pe1" style="font-size:17px;"><a href="{{ route('profile', $user->username) }}">{{ $user->name }} </a></li>
								<li class="pe1 pe2"><a href="#">{{ $user->post->count() }} Pictales</a></li>
								<li class="pe1 pe2"><a href="#">{{ $user->myfollowers()->count() }} followers</a></li>

						</ul>

				</li>
			</div>

					@endforeach
				</ul>

				</ul>
			</div>

			<div class="pictales_to_read">

						@foreach($posts as $post)
			 				@include('partials.post-draft')
			 		 @endforeach

			</div>
  </div>
</div>

<style media="screen">

	.people_e{
		width: 200px;
	}
	.pe1{

		padding: 5px;
		font-size: 13px;
		display: flex;
		margin-top: -5px;
	}
	.pe2{
		margin-left: 1px;
	}
	.pe1 a{
		color: #656464 !important;

	}
	.tags_to_follow{
		margin-left: -5px;
	}
 .tags_to_follow ul li {
	 display: inline-block;
	 list-style: none;
 }

 .people_to_follow {

 }

 ul li {
	 list-style: none;
	 display: inline-flex;
 }

 .img_e{
	 width: 70px;
 	height: 70px;
 }

 .pictales_to_read{
	 margin-top: -58px;
     margin-left: -5px;
 }
 .tags-container {
	 height: 37px;
 }
</style>
