<style type="text/css">
    .content{
        width: 100%;
    }
</style>

<div class="login">
    <div class="loginform">
        <form role="form" method="POST" action="{{ url('/auth/login') }}">
            <div class="formgroup" style="    width: 288px;
    margin-left: -18px;">
              <div class="text-holder">
                <input type="email" class="text {{ $errors->has('email') ? ' has-error' : ''}}" name="email" placeholder="email" value="{{ old('email') }}">
                <span class="underline"></span>
              </div>
              <div class="text-holder">
                <input type="password"class="text {{ $errors->has('password') ? ' has-error' : ''}}" name="password" placeholder="password">
                <span class="underline"></span>
              </div>
            </div>
            <div class="formgroup" style="display:flex;margin-top:6px;margin-left: -19px;
    width: 288px;">

                <div class="rem-me">
                    <input type="checkbox" name="remember" checked="true">Remember Me</input>
                </div>
                <div class="forgot-pass">
                    <a href="{{ url('/password/email') }}">I forgot my password</a>
                </div>
            </div>
            <button type="submit" class="login-btn">Log In</button>


            <div class="social_auth">
            <span>or log in with</span>
            <div class="sa1">
                <ul>
                <a href="{{ url('/login/facebook') }}"><li style="margin-right: 20px;"><img src="{{asset('images/fb_small.png')}}"></li></a>
                <a href="{{ url('/login/google') }}"><li><img src="{{asset('images/g_small.png')}}"></li></a>
                </ul>
            </div>
            </div>
            <input type="hidden" name="_token" value="{{ csrf_token() }}" style="clear:both;">
        </form>
        @if (count($errors) > 0)
        <div class="error-msg">
                <strong>Whoops!</strong> There were some problems with your input.<br><br>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
        </div>
        @endif
    </div>
</div>
