@extends('layout.main')
 @section('metatag')
    <title>{{ $title }}</title>
    <meta content="{{$title}}" property="og:title" />
    <meta content="{{$metaImg}}" property="og:image" />
  @endsection
@section('metatags')
  <meta content="{{$title}}" property="og:title" />
  <meta content="{{$metaImg}}" property="og:image" />
  <title>{{$title}}</title>

@endsection
@section('content')

@include('partials.profile-draft-for-follow')
<div class="follow-container">
  @if(!$user->myfollowers()->count())
    Forever alone
  @else
    @foreach($user->myfollowers()->get() as $user)
      <div class="user-profile">
        <div class="user-thumbnail">
            <a href="{{ route('profile', $user->username) }}">
              <img src="{{ url($user->profile->display_thumb_pic) }}" alt="display picture" />
            </a>

            <a href="{{ route('profile', $user->username) }}">
              <div class="user-name">
                {{ $user->name }}
              </div>
            </a>

          <div class="pictales-count">
            {{ $user->post->count() }} Pictales
          </div>
          <div class="user-follow-unfollow-btn">
            @unless($user->id == Auth::user()->id)
            @if(Auth::user()->isFollowing($user))
                <div class="unfollow-button" id="{{ $user->username }}">
                  <a href="{{ route('unfollow', $user->username)}}">Unfollow</a>
                </div>
                <div class="follow-button hide" id="{{ $user->username }}">
                  <a href="{{ route('follow', $user->username) }}">Follow</a>
                </div>

            @else
                <div class="unfollow-button hide" id="{{ $user->username }}">
                  <a href="{{ route('unfollow', $user->username)}}">Unfollow</a>
                </div>
                <div class="follow-button" id="{{ $user->username }}">
                  <a href="{{ route('follow', $user->username) }}">Follow</a>
                </div>
            @endif
            @endunless

        </div>
      </div>
    </div>

    @endforeach

@endif

</div>
@endsection
