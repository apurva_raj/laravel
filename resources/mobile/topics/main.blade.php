@extends('layout.main')

@section('metatag')

@endsection

@section('content')

@if(!Auth::check())
@include('partials.unauth_header')
<style media="screen">
	.sign-up {
		margin-top:-37px;
	}
</style>
@endif

  <div class="container tag_m">
		<div class="main">
			<div class="row topic_cover_pic">
				<img src="{{ url($topic->topic_cover_pic)}}" alt="cover_pic">
			</div>
			<span class="main_topic_style">{{ $topic->topic_name}} </span>

			<div class="row tt">
				<div class="topic_about">
					<p><span>{{$topic->topic_about}}</span></p>
				</div>
				<div class="topic_t">
					<ul style="margin-left:-37px;">
						<li><div class="ss1">{{$topic->post->count() }} pictales</div></li>
						<li><div class="ss1">{{ $topic->topicfollowers->count() }} Followers</div></li>
						@if(Auth::check())<li style="float:right">
							<div class="follow-unfollow-btn-pos-t topic_follow">


									@if($user->isFollowingTopic($topic))

									<div class="unfollow-button blue" id="{{ $topic->id }}">
										<a href="{{ route('unfollow_topic', $topic->id)}}"> Following {{$topic->topic_name}}</a>
									</div>
									<div class="follow-button hide gray"  id="{{ $topic->id }}">
										<a href="{{ route('follow_topic', $topic->id) }}"> Follow</a>
									</div>

									@else
									<div class="unfollow-button hide blue" id="{{ $topic->id }}">
										<a href="{{ route('unfollow_topic', $topic->id)}}"> Following {{$topic->topic_name}}</a>
									</div>
									<div class="follow-button gray" id="{{ $topic->id }}">
										<a href="{{ route('follow_topic', $topic->id) }}"> Follow</a>
									</div>

									@endif
							</div>
							</li>
						@endif
					</ul>
				</div>

				<!-- <div class="topics_to_combine">
{{ route('follow_topic', $topic->id)}}

					<div class="tc1">
						Combine with
					</div>
					<ul class="someclass">
						<a href="#" class="tt1"><li>Life lessons (5)</li></a>
						<a href="#" class="tt1"><li>Reviews (8)</li></a>
						<a href="#" class="tt1"><li>Behind the scenes (10)</li></a>
					</ul>
				</div> -->

				<!-- <div class="testing">
					<form class="search-form" role="form" method="GET" action="{{ url('/search/topic') }}" enctype="multipart/form-data"
>
						<input type="text" name="search" placeholder="Search..."  autocomplete="off" />
						<input type="hidden" name="_token" value="{{ csrf_token() }}">

						<button type="submit" value="Submit">Submit</button>

					</form>
					<div class="search-dropdown">
						<ul></ul>
					</div>
				</div> -->
			</div>
		</div>



  </div>
<div class="" style="padding-bottom: 30px;">
	@foreach ($posts as $post)
		@include('partials.post-draft')
	@endforeach

</div>

@endsection


<style media="screen">

	.main {
		color: #000;
	}

	.topic_cover_pic{
		padding-bottom: 13px;
	}
	.topic_cover_pic img {
		position: relative;
		width: 100%;
		height: 130px;
	}
	.topic_title{
		z-index: 100;
		font-size: 27px;
		position: relative;
	}
	.topic_t ul li {
		display: inline-block;
		list-style: none;
	}

	.container {
		position: relative;
	}

	ul li {
		list-style: none;
		display: inline;
	}

	.tt1{
		font-weight: 400;
font-style: normal;
color: rgba(0,0,0,.44);
font-size: 13px;
letter-spacing: 0;
text-decoration: none;
background: #fafafa;
border-radius: 3px;
border: 1px solid #f0f0f0;
padding: 5px 10px;
margin-right: 3px;

}

.main_topic_style{
	font-weight: 400;
    font-style: normal;
		color: rgba(0, 0, 0, 0.69);
    font-size: 27px;
    letter-spacing: 0;
    text-decoration: none;
		background: rgba(214, 214, 214, 0.32);
    border-radius: 3px;
    padding: 5px 10px;
		cursor: pointer;
}
	.tt1:hover {
		color: rgba(0,0,0,.8);
    border-color: rgba(0,0,0,.15);
    background: #fff;
    text-decoration: none;
	}
	.main_topic_style:hover{
	    border-color: rgba(0,0,0,.15);
	    background: #fff;
	    text-decoration: none;
	}

	.ss1{
		margin-right: 10px;
		font-size: 15px;
    color: #7e7e7e;
	}
	.topics_to_combine {
		margin-top: 10px;
	}
	.someclass{
		margin-top: -20px;
		margin-left: 68px;
	}

	.topic_follow{
		margin-top: -4px;
		font-size: 15px;

		float: right;
		margin-right: 15px;
    border-radius: 7px;
		cursor: pointer;
	}
	.topic_follow a {
		color: #7e7e7e;
	}

</style>
