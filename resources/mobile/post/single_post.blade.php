@extends('layout.main')

@section('metatag')
    <title>{{ $title }}</title>
    <meta content="{{$title}}" property="og:title" />
    <meta content="{{$metaImg}}" property="og:image" />
@endsection

@section('content')
@if(!Auth::check())
@include('partials.unauth_header')
@endif

<div class="container" style="margin:40px 0px;padding:5px;">
  <div class="inner-container">
    @include('partials.post-draft')
  </div>
  @if(!Auth::check())
  <div class="formore">
    <a href="{{  route('welcome') }}">For more, Sign up.</a>
  </div>
  @endif
</div>
@endsection
