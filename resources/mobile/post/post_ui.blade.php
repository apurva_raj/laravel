<div class="scroll">
	<ol>
		@if(!$posts->count())
      <p>There is nothing to show</p>
    @else
			@foreach ($posts as $post)
				@include('partials.post-draft')
			@endforeach
			<div class="page">
			{!! str_replace('/?', '?', $posts) !!}
			</div>
		@endif
	</ol>
</div>
