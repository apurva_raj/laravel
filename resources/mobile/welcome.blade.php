@extends('layout.main')
@section('metatag')
    <title>{{ $title }}</title>
    <meta content="{{$title}}" property="og:title" />
    <meta content="{{$metaImg}}" property="og:image" />

@endsection
@section('css')

<link href="{{ asset('css/home-mobile.css') }}" rel="stylesheet">
@endsection
	@section('home')

		<div class="container" style="width: 100%;">
	<div id="dialog" title="Contact form" class="login-right">
	   <a id="go" rel="leanModal" name="test" href="#login-popup">Log In</a>
	</div>

	<div id="login-popup">
			@include('auth.login')
	</div>


			<div class="centered">
				<img class= "centerlogo" src="{{ asset('images/pictales-logo-by-hetu_archi.png') }}" alt="Pictales" />
			</div>


			 <div class="tagline">
        		<div class="tl1">
        			<span>About almost everything that matters</span>
        		</div>

        		<div class="vision-button">
        			<a href="{{ URL::route('about') }}"><div class="vb1">Know more</div></a>
        		</div>
        		<div id="home_text" class="ht">
							<div class="hp2">Have something to share that matters?<a href="#">click here</a> to get an invite to join.</div>
        		</div>
            <div class="home_e">
              <a href="{{ route('explore') }}"><i class="material-icons" style="font-size:46px;color:#656464;">explore</i></a>
            </div>
    		</div>

    		<div class="invite centered">
			  <form role="form" method="POST" action="{{ url('/request') }}">
			    <input type="email" class="text" name="email" placeholder="email" value="{{ old('email') }}">
			    <button type="submit" class="btn invitebtn" style="left: 22px;">Request Invite</button>
			    <input type="hidden" name="_token" value="{{ csrf_token() }}">
			  </form>
			</div>


    		<div class="back"><button type="submit"class="btn backbtn"> Go Back </button></div>
		</div>
	@endsection
