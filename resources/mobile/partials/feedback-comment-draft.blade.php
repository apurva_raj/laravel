<div class="">
  @foreach($feedbacks as $feedback)
  <ul>
    <li>{{ $feedback->comment }}</li>
    <span class="count"><a href="">{{ $feedback->votes->count()}} </a></span>
    @if(Auth::user()->id == $feedback->user_id)
      <li class="adelete-button" id="{{ $feedback->id }}" >
        <a href="{{ route('suggetion_delete_path', ['feedbackId' => $feedback->id]) }}">Delete Post</a>
      </li>
    @endif
    @unless(Auth::user()->id == $feedback->user_id)
       @if(Auth::user()->hasVoted($feedback))
         <li class="" id="">
           <a href="{{ route('unvote_path', ['feedbackId' => $feedback->id]) }}">UnVote</a>
         </li>
         @else
         <li class="" id="">
          <a href="{{ route('vote_path', ['feedbackId' => $feedback->id]) }}">Vote</a>
         </li>
         @endif
    @endunless

  </ul>
  @endforeach
</div>
