<li class="comments-holder">
  <ul>
    <li>
      <div class="profile-menu small_dp">
        <a href="{{ route('profile', $comment->user->username) }}">
          <img src="{{ url($comment->user->profile->display_thumb_pic) }}"  alt="" />
        </a>
        <div class="author-name-comment">
          <span>{{$comment->user->name}}</span>
        </div>
      </div>
    </li>
    <li><div class="comment-style">{!!($comment->comment)!!}</div></li>
    <li><div class="carbon-date" style="margin-left: 29px;">{{ $comment->created_at->diffForHumans() }}</div></li>
    @if(Auth::check() && $comment->user_id == Auth::user()->id)<li><div class="delete-comment right" data-title="Delete Post" data-content="Are you sure you want to delete this comment?" onClick="return false;">
      <a href="{{ route('comment_delete_path', ['commentId' => $comment->id]) }}">delete</a></div>
    </li>
    @endif
</ul>
</li>
