@if(!Auth::check())
@include('partials.unauth_header')
@endif

<div class="container">

	<div class="profile-holder">
		<div class="profile-info">
			<div class="profile-p1">
		       	<img src="{{	url($user->profile->display_pic)	}}" alt="Profile pic">
						@if(Auth::check())
						<div class="follow-unfollow-btn-pos">
							@unless($user->id == Auth::user()->id)
								@if(Auth::user()->isFollowing($user))
										<div class="unfollow-button" id="{{ $user->username }}">
											<a href="{{ route('unfollow', $user->username)}}">Unfollow</a>
										</div>
										<div class="follow-button hide" id="{{ $user->username }}">
											<a href="{{ route('follow', $user->username) }}">Follow</a>
										</div>

								@else
										<div class="unfollow-button hide" id="{{ $user->username }}">
											<a href="{{ route('unfollow', $user->username)}}">Unfollow</a>
										</div>
										<div class="follow-button" id="{{ $user->username }}">
											<a href="{{ route('follow', $user->username) }}">Follow</a>
										</div>
								@endif
							@endunless
						</div>
						@endif

			</div>
			<div class="profile-p2">
				<div class="display-name">
					<span>{{ $user->name}}</span>
					@if(Auth::check() && Auth::user()->id === $user->id)
						<div class="profile-edit right">
						<a href="{{ route('profile.edit') }}"><img src="{{ asset('images/edit-icon.png') }}" alt="" class="prof-edit"/></a>
						</div>
					@endif
				</div>
				<div class="status">
					<span>{{ $user->profile->status }}
				</div>
				<div class="social-linked-icons">

					@if($user->profile->facebook_url)
					<a href="{{ $user->profile->facebook_url }}" target="_blank"><div class="fb-link"></div></a>
					@endif
					@if($user->profile->instagram_url)
					<a href="{{ $user->profile->instagram_url }}" target="_blank"><div class="insta-link"></div></a>
					@endif
					@if($user->profile->youtube_url)
					<a href="{{ $user->profile->youtube_url }}" target="_blank"><div class="yt-link"></div></a>
					@endif
					@if($user->profile->twitter_url)
   				<a href="{{ $user->profile->twitter_url }}" target="_blank"><div class="twitter-link"></div></a>
					@endif
				</div>
			</div>



			<div class="profile-p3">
				<div class="short-bio">
					<p>{{ $user->profile->bio }}</p>
				</div>
			</div>

			<div class="statistics">
				<div class="stats-pictales">
					<span>{{ $user->post->count() }} Pictales</span>
				</div>
				<div class="stats-followers">
					<a href="{{ route('followers', $user->username) }}"><span>{{ $user->myfollowers()->count() }} Followers</span></a>
				</div>
				<div class="stats-following">
					<a href="{{ route('following', $user->username) }}"><span>{{ $user->ImFollwing()->count() }} Following</span></a>
				</div>
			</div>
		</div>
	</div>


	<div class="profile-v1">
	  <div class="content">
	    <div class="" style="margin-bottom:40px">
	        <div class="inner-container">
							<div class="post-v2">
						      @include('post.post_ui')
							</div>
						</div>
	    </div>
	  </div>
	</div>

</div>
