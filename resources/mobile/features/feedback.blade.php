@extends('layout.main')

 @section('metatag')
    <title>{{ $title }}</title>
    <meta content="{{$title}}" property="og:title" />
    <meta content="{{$metaImg}}" property="og:image" />
  @endsection

@section('content')
  <div class="cd1 feedback_container">
    <h3>Your Feedback is valuable to us!</h2>
    <div class="features">
      <p>Upcoming site features:</p>
      <ul>
        <li>- Ability to search posts</li>
        <br><li>- Topics to tag</li>
      </ul>
      <p id="feedback">Please leave us a feedback below and tell us what you like, dont like or would like to see here in the future.</p><br>
    </div>
    <div class="feedback-input">
      <form role="form" action= "{{ route('suggestion_path') }}" method="post">
          <textarea name="comment_aws" rows="8" cols="40" placeholder="Type feedback here..." id="fbtext"></textarea>
          <br>
          <input class = "btn feedback_button " type="submit" value="Feedback"/>
          <input type="hidden" name="_token" class="token"value="{{ Session::token() }}"/>
      </form>
    </div>
    <div class="comments">
      @include('features.feedback_comment')
    </div>
  </div>
</div>

@endsection
