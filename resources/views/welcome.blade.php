@extends('layout.main')
@section('metatag')
    <title>{{ $title }}</title>
    <meta content="{{$title}}" property="og:title" />
    <meta content="{{$metaImg}}" property="og:image" />

@endsection
@section('css')

<link href="{{ asset('css/home.css') }}" rel="stylesheet">
<style media="screen">
.topbar-container {
  display: none !important;
}
</style>
@endsection
	@section('home')

		<div class="container" style="width: 100%; min-width: 800px;">
			<div class="centered">
				<img class= "centerlogo" src="{{ asset('images/pictales-logo-by-hetu_archi.png') }}" alt="Pictales" />
				<br>
				@include('auth.login')


			</div>

			 <div class="tagline">
        		<div style="font-size: 53px;">
        			<span>About almost everything that matters</span> <br>

        		</div>

        		<div class="vision-button">
        			<a href="{{ URL::route('about') }}"><div class="vb1">Know more</div></a>

            </div>
        		<div id="home_text" class="ht">
        			<div class="hp2">Have something to share that matters? <a href="#">click here</a> to get an invite to join.
        		</div>
            <div class="" style="margin-top:23px;">
              <a href="{{ route('explore') }}"><i class="material-icons" style="font-size:46px;color:#656464;">explore</i></a>
            </div>
      </div>


    		<div class="invite centered">
			  <form role="form" method="POST" action="{{ url('/request') }}">
			    <input type="email" class="text" name="email" placeholder="email" value="{{ old('email') }}">
			    <button type="submit" class="btn invitebtn" style="left: 22px;">Request Invite</button>
			    <input type="hidden" name="_token" value="{{ csrf_token() }}">
			  </form>
			</div>


    		<div class="back"><button type="submit"class="btn backbtn"> Go Back </button></div>
		</div>
	@endsection
