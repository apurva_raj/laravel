@extends('layout.main')

@section('metatag')
    <title>{{ $title }}</title>
    <meta content="{{$title}}" property="og:title" />
    <meta content="{{$metaImg}}" property="og:image" />
@endsection


@section('content')
  <div class="cd1 feedback_container">
    <div class="features">
      <p id="upcoming"><b>UPCOMING FEATURES</b></p>
      <ul class="l">
        <br><li>- Ability to search posts</li>
        <br><li>- View counts</li>
        <br><li>- New text editor and new layout</li>

      </ul>
      <p id="feedback">Please leave us a feedback below on what you like, don't like or would like to see on this website.</p><br>
    </div>
    <div class="feedback-input">
      <form role="form" action= "{{ route('suggestion_path') }}" method="post">
          <div class="imgbar"><img src="{{ url(Auth::user()->profile->display_pic) }}" alt="" /></div>
          <textarea name="comment_aws" rows="8" cols="70" placeholder="Type feedback here..." id="fbtext"></textarea><br>

          <input class = "btn feedback_button " type="submit" value="Feedback"/>
          <input type="hidden" name="_token" class="token"value="{{ Session::token() }}"/>
      </form>
    </div>
    <div class="comments">
      @include('features.feedback_comment')
    </div>
  </div>
</div>

@endsection
