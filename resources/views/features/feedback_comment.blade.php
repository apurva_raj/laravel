<ul>
@foreach($feedbacks as $feedback)
  <li class="feedback-holder">
    <ul>
      <li>
        <div class="profile-menu small_dp">
          <a href="{{ route('profile', $feedback->user->username) }}">
            <img src="{{ url($feedback->user->profile->display_thumb_pic) }}"  alt="" />
          </a>

          <div class="author-name-comment" style="color:#686868;">
            <span>{{$feedback->user->name}}, {{ $feedback->created_at->diffForHumans() }}</span>
          </div>
        </div>
      </li>
      <li>
        <div class="feedback-style">{!!($feedback->comment)!!}</div>
      </li>
      <li><div class="carbon-date" style="margin-left: 28px;"</div></li>
      <span class="feedback-count"> {{ $feedback->votes->count()}} Votes</span>
    @if(Auth::user()->id == $feedback->user_id)
      <li class="adelete-button" id="{{ $feedback->id }}" >
        <a href="{{ route('suggestion_delete_path', ['feedbackId' => $feedback->id]) }}">Delete Post</a>
      </li>
    @endif
    @unless(Auth::user()->id == $feedback->user_id)
     @if(Auth::user()->hasVoted($feedback))
       <li class="feedback-vote" id="">
         <a href="{{ route('unvote_path', ['feedbackId' => $feedback->id]) }}">Unvote</a>
       </li>
       @else
       <li class="feedback-vote" id="">
        <a href="{{ route('vote_path', ['feedbackId' => $feedback->id]) }}">Vote</a>
       </li>
       @endif
    @endunless
    </ul>
  </li>
@endforeach
</ul>
