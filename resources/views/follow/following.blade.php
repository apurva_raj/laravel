@extends('layout.main')

@section('metatag')
    <title>{{ $title }}</title>
    <meta content="{{$title}}" property="og:title" />
    <meta content="{{$metaImg}}" property="og:image" />
@endsection

@section('content')

@include('partials.profile-draft-for-follow')
      <div class="follow-container">

      @if(!$user->ImFollwing()->count())
        Forever alone
      @else
          @include('partials.followers-list')
      @endif
      </div>

@endsection
