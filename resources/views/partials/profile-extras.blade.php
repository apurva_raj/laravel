<div class="profile-v1">
  <div class="content">
    <div class="">
        @if(Auth::user()->user_id === $user->user_id)
          <div class="inner-container">
              @include('post.update_post')
          </div>
        @endif
        @if(!$posts->count())
          <div class="inner-container">
            <p>There is nothing to show</p>
          </div>
        @else
          <div class="inner-container">
            @include('post.post_ui')
          </div>
        @endif
    </div>
  </div>
</div>
