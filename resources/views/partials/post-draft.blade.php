
<div class="post-container pc2">

  <div class="tags-container">
      <ul>
        @foreach ($post->topics as $topic)
        <a href="{{ route('topic_name', $topic->topic_slug_name) }}"><li class="tag">{{$topic->topic_name}}</li></a>
        @endforeach
      </ul>
  </div>


  <div class="title-container">
    <b>
      <a href="{{ route('pictales.name', [$post->user->username, $post->slug_title]) }}">{{ $post->title }}</a>
    </b>
  </div>

  <div class="pictales-author profile-menu">
    <div class="img-container">
      <a href="{{ route('profile', $post->user->username) }}">
        <img src="{{ url($post->user->profile->display_thumb_pic) }}" alt="" />
      </a>
    </div>

    <a href="{{ route('profile', $post->user->username) }}">
      <div class="author-name">
        <span> {{$post->user->name}}, </span> <span style="font-size: 13px;">{{ $post->created_at->diffForHumans() }} </span>
      </div>
    </a>
  </div>
  <div class="story-container {{$readmore}}" id="ms" style="height:100% !important;">

    <div style="margin-top-5px;" class="ss-c" id="post_{$post->id}">{!! $post->post_story !!}</div>

  </div>
  <div class="image-container-s" id="post_{{ $post->id }}">
      <img src="{{ url($post->file_path) }}" alt="">
  </div>


<style media="screen">
  .story-container{
    height: 100%;
  }
  .image-container-s {
    width: 100%;

    margin-top: -10px;
  }


</style>



  <div class="options" style="dis">
    <ul>
      <div style="margin-top: -7px;">
        <div class="view_counts">
          {{ Counter::showAndCount('pictales.name', $post->user->username, $post->slug_title) }} Views
        </div>
        @if(Auth::check())
        @if(Auth::user()->id == $post->user_id)
          <li>
            <span>{{ $post->likes->count() }}</span>  {{ str_plural('Commend', $post->likes->count()) }}</a>
          </li>
         @else
            @if(Auth::user()->hasLiked($post))
            <li class="unlike-button active" id="{{ $post->id }}">
              <a href="{{ route('unlike_path', ['postId' => $post->id]) }}"><span>{{ $post->likes->count() }}</span>  {{ str_plural('Commend', $post->likes->count()) }}</a>
            </li>
            <li class="post-like hide" id="{{ $post->id }}">
              <a href="{{ route('like_path', ['postId' => $post->id]) }}"><span>{{ $post->likes->count() }}</span>  {{ str_plural('Commend', $post->likes->count()) }}</a>
            </li>
            @else
            <li class="unlike-button hide" id="{{ $post->id }}">
              <a href="{{ route('unlike_path', ['postId' => $post->id]) }}"><span>{{ $post->likes->count() }}</span>  {{ str_plural('Commend', $post->likes->count()) }}</a>
            </li>
            <li class="post-like" id="{{ $post->id }}">
              <a href="{{ route('like_path', ['postId' => $post->id]) }}"><span>{{ $post->likes->count() }}</span>  {{ str_plural('Commend', $post->likes->count()) }}</a>
            </li>
            @endif
        @endif
        @else
        <li>
          <span>{{ $post->likes->count() }}</span>  {{ str_plural('Commend', $post->likes->count()) }}</a>
        </li>
        @endif
        <li>
          <div class="show-comments" style="margin-left: 4px;">
            <span class="count"><a href="">{{ $post->comments->count()}} {{ str_plural('Reply', $post->comments->count()) }}</a></span>
          </div>
        </li>


        @if(Auth::check() && Auth::user()->id == $post->user_id)
        <li class="opt-menu right"><a>...</a>
          <div class="opt-dropdown">
            <ul>
              <li class="delete-button" id="{{ $post->id }}" data-title="Delete Post" data-content="Are you sure you want to delete this post?" onClick="return false;">
                <a href="{{ route('pictales.delete', ['postId' => $post->id]) }}">Delete Post</a>
              </li>
            </ul>
         </div>
        </li>
        @endif
        <li style="float:right">
          <div id="shares">
            <a class="popup" href="https://www.facebook.com/sharer/sharer.php?u={{ route('pictales.name', [$post->user->username, $post->slug_title]) }}">
              <img src="https://cdnjs.cloudflare.com/ajax/libs/webicons/2.0.0/webicons/webicon-facebook-s.png" class="share-img">
            </a>
            <a class="popup" href="https://twitter.com/home?status={{ route('pictales.name', [$post->user->username, $post->slug_title]) }}">
              <img src="https://cdnjs.cloudflare.com/ajax/libs/webicons/2.0.0/webicons/webicon-twitter-s.png" class="share-img">
            </a>
            <button class="btns" data-clipboard-text="{{ route('pictales.name', [$post->user->username, $post->slug_title]) }}">
            <img src="https://cdn0.iconfinder.com/data/icons/web/512/e28-128.png" class="share-img" id="copy">
            <div id="hide">
              <div class="popover-clipboard" style="float: right;margin-right: 20px;
                margin-top: -23px"></div>
                <div>Copied!</div>
            </div>
            </button>
          </div>
        </li><br><br>



      </div>

    </ul>
  </div>

  <div class="comments sh-comments">
    <ul>
    @foreach($post->comments as $comment)
      @include('partials.comment-draft')
    @endforeach
    </ul>
    @if(Auth::check())
    <form action="{{ route('comment_path', ['postId' => $post->id]) }}" id="{{ $post->id }}" method="post">
      <div class="comment-container" id="{{ $post->id }}">
        <div class="imgbar" style="float:left;"><img src="{{ url(Auth::user()->profile->display_thumb_pic) }}" alt="" /></div>
        <div class="comment-text text-edit" id="comments" contentEditable="true" data-ph="Write a comment..."></div>
        <input type="submit" value="Comment" class="comment-btn" style="float:right;"/>
        <input type="hidden" name="_token" class="token"value="{{ Session::token() }}" style="clear:both;"/>
      </div>
    </form>
    @endif
  </div>

</div>
