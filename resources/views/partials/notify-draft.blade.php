

<link href="{{ asset('/css/notify.css') }}" rel="stylesheet">
<link href="{{ asset('/css/stylesheet.css') }}" rel="stylesheet">
<script src="{{ asset('/js/jquery.min.js') }}" type="text/javascript"></script>
<script type="text/javascript" src="{{ asset('/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('/js/dropdown.js') }}" type="text/javascript"></script>
<script type="text/javascript">var pathToGif = "{{ asset('/images/103.gif') }}";</script>
<script type="text/javascript" src="{{ asset('/js/jscroll/jquery.jscroll.js') }}"></script>
<div class="notif-dropdown">
  <div class="menu-container">
      	<ol>
          @if($notify->count() < 0)
            @foreach($notify as $notification)
              <li>
                <a href=" {{ $notification['attributes']['url'] }} "> {{ $notification['attributes']['text'] }}</a>

                <div class= "delete">x</div>
              </li>
            @endforeach
          @else
            <li>
              <a><i class="fa fa-bell-o"></i> Nothing to show.</a>
            </li>
          @endif
        </ol>
  </div>
  <div class="fixed-bottom">
    <div><a href="">See All</a></div>
  </div>
</div>
