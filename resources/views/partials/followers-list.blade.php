@foreach($user->ImFollwing()->get() as $user)

<div class="user-profile">
  <div class="user-thumbnail">
      <a href="{{ route('profile', $user->username) }}">
        <img src="{{ url($user->profile->display_thumb_pic) }}" alt="display picture" />
      </a>
         <div class="user-name">
            <a href="{{ route('profile', $user->username) }}">
            {{ $user->name }}
             </a>

         </div>
  <div class="pictales-count">
      {{ $user->post->count() }} Pictales
  </div>
  <div class="user-follow-unfollow-btn">
    @unless($user->id == Auth::user()->id)

    @if(Auth::user()->isFollowing($user))
        <div class="unfollow-button" id="{{ $user->username }}">
          <a href="{{ route('unfollow', $user->username)}}">Unfollow</a>
        </div>
        <div class="follow-button hide" id="{{ $user->username }}">
          <a href="{{ route('follow', $user->username) }}">Follow</a>
        </div>

    @else
        <div class="unfollow-button hide" id="{{ $user->username }}">
          <a href="{{ route('unfollow', $user->username)}}">Unfollow</a>
        </div>
        <div class="follow-button" id="{{ $user->username }}">
          <a href="{{ route('follow', $user->username) }}">Follow</a>
        </div>
    @endif
      @endunless
</div>
  </div>
</div>
@endforeach
