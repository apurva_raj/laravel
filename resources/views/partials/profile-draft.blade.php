
@section('tinymce')

<script type="text/javascript" src="{{ asset('/js/tinymce/tinymce.min.js') }}"></script>

<script type="text/javascript">
window.onload=function(){
tinyMCE.init({
  selector: "#postitle",
  menubar: false,
  toolbar: false,
  statusbar: false,
  plugins: "paste",
  paste_as_text: true,
  setup: function(ed){
    ed.on('keyup',function(e){
      var cnt=countCharsTitle();
      document.getElementById("titleLen").innerHTML=(150-cnt);
    });
  }
});
};
</script>
<script type="text/javascript">
window.onload=function(){
tinyMCE.init({
  selector: "#post_story",
  menubar: false,
  toolbar: "bold italic underline",
  statusbar: false,
  plugins: "paste autoresize",
  autoresize_bottom_margin: 7,
  editor_selector : "mceEditor",
  paste_as_text: true,

 setup: function(ed){
    ed.on('keyup',function(e){
      var count=countChars();
      document.getElementById("len").innerHTML=(2000-count);
    });
  }
});
};

function countChars() {
  var body = tinymce.get("post_story").getBody();
  var content = tinymce.trim(body.textContent);
  var len = $.trim(content).length;
  if(content.length>2000){
    document.getElementById("post_story_ifr").style.border="1px solid #b43636";
  }
  if(content.length<=2000){
    document.getElementById("post_story_ifr").style.border="none";
  }
  return len;
};
function countCharsTitle() {
  var b = tinymce.get("#post_title").getBody();
  var cntnt = tinymce.trim(b.innerText||b.textContent);
  var l = $.trim(cntnt.replace(/(<([^>]+)>)/ig, "")).length;
  if(cntnt.length>5){
    document.getElementById("post_title").style.border="1px solid #b43636";
  }
  if(cntnt.length<=5){
    document.getElementById("post_title").style.border="none";
  }
  return l;
};

</script>
@endsection

<div class="container ec1">

	<div class="profile-holder">
		<div class="profile-info">
			<div class="profile-p1">
		       	<img src="{{	url($user->profile->display_pic)	}}" alt="Profile pic">
						@if(Auth::check())
						<div class="follow-unfollow-btn-pos">
							@unless($user->id == Auth::user()->id)
								@if(Auth::user()->isFollowing($user))
										<div class="unfollow-button" id="{{ $user->username }}">
											<a href="{{ route('unfollow', $user->username)}}"> UNFOLLOW</a>
										</div>
										<div class="follow-button hide" id="{{ $user->username }}">
											<a href="{{ route('follow', $user->username) }}"> FOLLOW</a>
										</div>

								@else
										<div class="unfollow-button hide" id="{{ $user->username }}">
											<a href="{{ route('unfollow', $user->username)}}"> UNFOLLOW</a>
										</div>
										<div class="follow-button" id="{{ $user->username }}">
											<a href="{{ route('follow', $user->username) }}"> FOLLOW</a>
										</div>
								@endif
							@endunless
						</div>
						@endif
			</div>
			<div class="profile-p2">
				<div class="display-name">
					<span>{{ $user->name}}</span>

					@if( Auth::check() && Auth::user()->id === $user->id)
						<div class="profile-edit right">
						<a href="{{ route('profile.edit') }}"><img src="{{ asset('images/edit-icon.png') }}" alt="" class="prof-edit"/></a>
						</div>
					@endif

				</div>
				<div class="status">
					<span>{{ $user->profile->status }}
				</div>
				<div class="social-linked-icons">

					@if($user->profile->facebook_url)
					<a href="{{ $user->profile->facebook_url }}" target="_blank"><div class="fb-link"></div></a>
					@endif
					@if($user->profile->instagram_url)
					<a href="{{ $user->profile->instagram_url }}" target="_blank"><div class="insta-link"></div></a>
					@endif
					@if($user->profile->youtube_url)
					<a href="{{ $user->profile->youtube_url }}" target="_blank"><div class="yt-link"></div></a>
					@endif
					@if($user->profile->twitter_url)
   				<a href="{{ $user->profile->twitter_url }}" target="_blank"><div class="twitter-link"></div></a>
					@endif
				</div>

				<div class="statistics">
					<div class="stats-pictales">
						<span>{{ $user->post->count() }} Pictales</span></a>
					</div>
					<div class="stats-followers">
						<a href="{{ route('followers', $user->username) }}"><span>{{ $user->myfollowers()->count() }} Followers</span></a>
					</div>
					<div class="stats-following">
						<a href="{{ route('following', $user->username) }}"><span>{{ $user->ImFollwing()->count() }} Following</span></a>
					</div>
				</div>

			</div>
			<div class="profile-p3">
				<div class="short-bio">
					<p>{{ $user->profile->bio }}</p>
				</div>
			</div>

		</div>
	</div>

	<div class="profile-v1">
	    <div class="">
	        @if(Auth::check() && Auth::user()->id == $user->id)

					@section('tinymce')

					<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/tinymce/4.3.13/tinymce.min.js"></script>

					@endsection
	          <div class="inner-container">
							<div class="post-v2">
	              @include('post.update_post')
							</div>
						</div>
	        @endif
	          <div class="inner-container">
							<div class="post-v2">
						      @include('post.post_ui')
							</div>
						</div>
	    </div>
	</div>


</div>
