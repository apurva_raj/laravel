
@extends('layout.default')
@section('metatag')
    <title>{{ $title }}</title>
    <meta property="og:title" content="{{$title}}"  />
    <meta property="og:image" content="{{$metaImg}}"  />
    <meta property="og:description" content="{{$ds}}..."  />

@endsection

@section('public-post')
<body>

<div class="container">

@if(Auth::check())
@include('layout.header')
@else

  <div class="topbar-container">
      <div class="navigation">
        <div class="navbar">
            <div class="logo">
              <a href="{{ URL::route('welcome') }}"><span><img src="{{ asset('images/pictales-logo-by-hetu_archi.png') }}"></span></a>
            </div>
            <div class="sign-up" style="margin-top: 11px;">
              <a href="{{ route('welcome')}}">Sign up</a>
            </div>
        </div>
      </div>
  </div>
@endif
  <div class="inner-container" style="margin-left: 120px;">
    <div class="cd1">


    <div class="post-container">

      <div class="title-container">
        <b>
          <a href="{{ route('pictales.name', [$post->user->username, $post->slug_title]) }}">{{ $post->title }}</a>
        </b>
      </div>

      <div class="pictales-author profile-menu">
        <div class="img-container">
          <a href="{{ route('profile', $post->user->username) }}">
            <img src="{{ url($post->user->profile->display_thumb_pic) }}" alt="" />
          </a>
        </div>

        <a href="{{ route('profile', $post->user->username) }}">
          <div class="author-name">
            <span> {{$post->user->name}}, </span> <span style="font-size: 13px;">{{ $post->created_at->diffForHumans() }} </span>
          </div>
        </a>

      </div>

        <div class="story-container">
          <span>{!! $post->post_story !!}</span>
        </div>

        <div class="image-container">
          <a href="{{ url($post->file_path) }}">
            <img src="{{ url($post->file_path) }}" alt="">
          </a>
        </div>

        <div class="options">
          <ul>

                <div style="margin-top: -7px;">
                  <li>

                    <span>{{ $post->likes->count() }}</span>  {{ str_plural('Commend', $post->likes->count()) }}
                  </li>

                  <li>
                    <div class="show-comments" style="margin-left: 4px;">
                      <span class="count"><a href="">{{ $post->comments->count()}} {{ str_plural('Reply', $post->comments->count()) }}</a></span>
                    </div>
                  </li>

                  <li class="share-button right" style="margin-right: 10px;"id="{{ $post->id }}">
                    <a href="">Share link</a>
                  </li>
                </div>


                <li>
                  <div class="options share">
                    <span><b>public share link</b> <a href="{{ route('public_share_link', ['postId' => $post->share_md5]) }}">{{ route('public_share_link', ['postId' => $post->share_md5]) }}</a></span>
                  </div>
                </li>
          </ul>
        </div>
        <div class="comments sh-comments">
          <ul>
          @foreach($post->comments as $comment)
          <li class="comments-holder">
            <ul>
              <li>
                <div class="profile-menu small_dp">
                   <a href="{{ route('profile', $comment->user->username) }}">
                       <img src="{{ url($comment->user->profile->display_pic) }}"  alt="" />
                  </a>
                  <div class="author-name-comment">
                    <span>{{$comment->user->name}}</span>
                  </div>
                </div>
              </li>
              <li><div class="comment-style">{!!($comment->comment)!!}</div></li>
              <li><div class="carbon-date" style="margin-left: 28px;">{{ $comment->created_at->diffForHumans() }}</div></li>

          </ul>
          </li>

          @endforeach
          </ul>

        </div>
        @if(!Auth::check())
        <div class="formore">
          <a href="{{  route('welcome') }}">For more, Sign up.</a>
        </div>
        @endif

    </div>
  </div>

</div>

</div>

<link href="{{ asset('/css/ForPublicPosts.css') }}" rel="stylesheet">

</body>

@endsection
