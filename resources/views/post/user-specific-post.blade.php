@extends('layout.main')

@section('metatag')
    <title>{{ $title }}</title>
    <meta content="{{$title}}" property="og:title" />
    <meta content="{{$metaImg}}" property="og:image" />
@endsection

<link href="{{ asset('/css/profile.css') }}" rel="stylesheet">

@section('content')
@include('partials.profile-draft')
<div class="profile-v1" style="margin-top: 335px;">
    @if(!$posts->count())
      <p>There is nothing to show</p>
    @else
  <div class="inner-container post-position">
    @include('post.post_ui')
  </div>
    @endif
</div>
@endsection
