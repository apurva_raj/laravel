<div class="head">
	<div id="create">Create your Pictales</div>
</div>


<div class="input-holder" id="hidden">

		<div class="update-post">
			<form role="form" action="{{ route('pictales.add') }}" id="upload_form" method="post" file="true"  enctype="multipart/form-data">
				<div class="" id="">
					<div id="a">
						<div contentEditable="true" id="post_title" name="q" data-ph="Add title"></div>
						<div id="textarea_feedback_title" class="hide"></div>
						<div class="has-error-text error-input-title"></div>
					</div>
					<div id="c" >
						 <textarea id="post_story" class="aC mceEditor" name="post_story" placeholder="Write story here">Tell your story</textarea>
						 	<span class="has-error-text error-input-story"></span>
							<div id="len" style="float:right; font-size:15px"></div>
					</div>
				</div>
		</div>
			<div class="post-buttons">

				<label for="file_img" class="input-img"><img id ="file_preview" src=" {{ asset('images/plus_icon.png') }}" style="height:50px;width:50px"></label>
				<span class="has-error-text error-input-img"></span>
				<input type="file" name="file_img" id="file_img" class="upload-btn">

				<img src="{{ asset('images/processing.gif') }}" class="submit-loading">
				<input type="submit" value="Upload" name="submit" class="submit-btn" id="post-pictales">
			</div>
			<input type="hidden" name="_token" class="token" value="{{ Session::token() }}">
		</form>

		<!-- <div class="control-group">
  		<select id="select-topics" class="selc-topics" placeholder="Add topics...">
    		<option value="">Type your name ...</option>
  		</select>
		</div> -->




		<div class="search-dropdown">
			<ul></ul>
		</div>


</div>
