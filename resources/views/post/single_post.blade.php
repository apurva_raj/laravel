@extends('layout.main')

@section('metatag')
    <title>{{ $title }}</title>
    <meta content="{{$title}}" property="og:title" />
    <meta content="{{$metaImg}}" property="og:image" />
@endsection

@section('content')
@if(!Auth::check())
@include('partials.unauth_header')
@endif

<div class="container ec1">
  <div class="leftbar leftbarfix clearfix">
    @if(Auth::check())
    @include('layout.leftbar')
    @endif
  </div>
  <div class="single_post_container_fix">
    @include('partials.post-draft')
  </div>
  @if(!Auth::check())
  <div class="formore">
    <a href="{{  route('welcome') }}">For more, Sign up.</a>
  </div>
  @endif
</div>
@endsection
