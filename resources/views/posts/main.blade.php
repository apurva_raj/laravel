@extends('layout.main')

<link href="{{ asset('/css/profile.css') }}" rel="stylesheet">

@section('content')
@include('partials.profile-draft')
<div class="profile-v1" style="margin-top: 398px;">
  <div class="inner-container post-position">
    @include('post.post_ui')
  </div>
</div>
@endsection
