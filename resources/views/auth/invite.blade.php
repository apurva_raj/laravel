
@extends('layout.default')
@section('metatag')
    <title>{{ $title }}</title>
    <meta content="{{$title}}" property="og:title" />
    <meta content="{{$metaImg}}" property="og:image" />
@endsection

<body>
	@if (Auth::check())
	@include('layout.header')

		<div class="invite-user centered">
			<div class= "count"><span style="margin: 5px;"><b>Invites left : </b></span>{{ $count }}
			<div class="invite-holder">
					<form role="form" method="POST" action="{{ route('mailer.invite') }}">
						<input type="email" class="text {{ $errors->has('email') ? ' has-error' : ''}}" name="email" placeholder="Enter E-mail to send invite" value="{{ old('email') }}">
						<button type="submit" class="btn invitebtn">Send Invite</button>
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
					</form>
			</div>
			@if (count($errors) > 0)
				<div class="error-msg">
					<strong>Whoops!</strong> There were some problems with your input.<br><br>
					<ul>
						@foreach ($errors->all() as $error)
							<li>{{ $error }}</li>
						@endforeach
					</ul>
				</div>
			@endif
		</div>
	@else

	@endif
</body>
