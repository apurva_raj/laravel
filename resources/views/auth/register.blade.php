@include('layout.default')


<div class="flashholder">
	@include('layout.flash')
</div>
<div class="container">
	<div class="centered" style="width:70%;min-width: 800px;">
		<img class= "centerlogos" src="{{asset('images/pictales-logo-by-hetu_archi.png') }}" alt="Pictales" />
		<br>
		@if (count($errors) > 0)
			<div class="error-msg">
				<strong>Whoops!</strong> There were some problems with your input.<br><br>
				<ul>
				@foreach ($errors->all() as $error)
					<li>{{ $error }}</li>
				@endforeach
				</ul>
			</div>
		@endif
		<form role="form" method="POST" action="{{ url('/auth/register') }}">
			<div class="formgroup">
				<input type="email" class="text_n {{ $errors->has('email') ? ' has-error' : ''}}" name="email" value="{{ $email }}" readonly="readonly">
			</div>
		<div class="formgroup">
			<input type="text" class="text_n {{ $errors->has('name') ? ' has-error' : ''}}" name="name" placeholder="Display name" value="{{ old('name') }}">
		</div>
		<div class="formgroup">
			<input type="password" class="text_n {{ $errors->has('password') ? ' has-error' : ''}}" name="password" placeholder="Password">
		</div>
		<div class="formgroup">
			<input type="password" class="text_n {{ $errors->has('password') ? ' has-error' : ''}}" name="password_confirmation" placeholder="Confirm password">
		</div>
		<div class="formgroup">
			<button type="submit" class="btn login-btn" style="margin-top:6px;margin-right: 367px;">Register</button>
		</div>
		<input type="hidden" name="_token" value="{{ csrf_token() }}" style="clear:both;">
		</form>



		<div class="signup" style="margin-top: 60px;">
			<div class="hr">
				<div class="hr-text">OR</div>
			</div>
			<div class="withgoogle">
				<a href="{{ url('/register/google') }}" role="dialog" class="btn google-btn">
					<span class="google_button_text">Sign Up With Google</span>
				</a>
			</div>
			<div class="withfacebook">
				<a href="{{ url('/register/facebook') }}" class="btn fb-btn">
					<span class="fb_button_text">Sign Up With Facebook</span>
				</a>
			</div>
		<!--<div class="withemail">
			<span><a href="">***Sign up with email</a></span>
		</div>-->
		</div>

				</div>
			</div>
		</div>
	</div>
</div>
