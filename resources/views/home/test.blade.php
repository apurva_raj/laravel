


<div class="">
  <div class="cd1-hp">
@foreach($posts as $post)

  <div class="post-container-hp">

    <div class="title-container">
      <b>
        <a href="{{ route('pictales.name', [$post->user->username, $post->slug_title]) }}">{{ $post->title }}</a>
      </b>
    </div>

    <div class="pictales-author profile-menu">
      <div class="img-container">
          <img src="{{ url($post->user->profile->display_thumb_pic) }}" alt="" />
      </div>

      <a href="{{ route('profile', $post->user->username) }}">
        <div class="author-name">
          <span> {{$post->user->name}}, </span> <span style="font-size: 13px;">{{ $post->created_at->diffForHumans() }} </span>
        </div>
      </a>

    </div>

      <div class="story-container sc1">
        <span>{!! $post->post_story !!}</span>
      </div>

      <div class="image-container-hp">
          <img src="{{ url($post->file_path) }}" alt="" data-src="{{ url($post->file_path) }}" class="lazy">
      </div>

  </div>

  	@endforeach
</div>

</div>

</div>

<link href="{{ asset('/css/ForPublicPosts.css') }}" rel="stylesheet">

</body>
