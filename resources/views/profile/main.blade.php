@extends('layout.main')

@section('metatag')
    <meta property="og:title" content="{{$title}}"  />
    <meta property="og:image" content="{{$metaImg}}"  />
    <meta property="og:description" content="{{$ds}}"  />

    <title>{{$title}}</title>
@endsection

@section('content')

      @include('partials.profile-draft')
      <link href="{{ asset('/css/profile.css') }}" rel="stylesheet">

@endsection
