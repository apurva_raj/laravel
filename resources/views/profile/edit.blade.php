@extends('layout.main')
<link href="{{ asset('/css/profile.css') }}" rel="stylesheet">

@section('metatag')
    <title>{{ $title }}</title>
    <meta content="{{$title}}" property="og:title" />
    <meta content="{{$metaImg}}" property="og:image" />
@endsection

@section('content')
	<div class="cd5">
		<div class="title">
			<span>Edit profile</span>

		</div>
		<div class="tray1">

			<form action="{{ route('profile.edit') }}" method="post" file="true" enctype="multipart/form-data">

			<div class="form-group">
					<div class="edit edit-dp">
						<img src="{{ url(Auth::user()->profile->display_pic) }}" alt="" />
						<input type="file" name="file_img" id="file" class="upload-btn" style="border:none;">
						@if ($errors->has('file_img'))
						<div class="has-error-text">{{ $errors->first('file_img') }}</div>
	                 	@endif
					</div>
					<div class=" edit edit-d-name">
					  <label for="name" class="control-label">Display name</label>
		        <input type="text" name="name" class="form-control" id="name" value="{{ Auth::user()->name ?:Request::old('name') }}">
				@if ($errors->has('name'))
					<div class="has-error-text error-input-profile">{{ $errors->first('name') }}</div>
                 @endif
					</div>
					<div class="edit edit-status">
						<label for="status" class="control-label">Status</label>
		        <input type="text" name="status" class="form-control" id="status" value="{{ Auth::user()->profile->status ?:Request::old('status') }}">
		        @if ($errors->has('status'))
					<div class="has-error-text error-input-profile">{{ $errors->first('status') }}</div>
                 @endif
					</div>
					<div class="edit edit-bio">
			   		<label for="bio" class="control-label">Bio</label>
		        <input type="text" name="bio" class="form-control" id="bio" value="{{ Auth::user()->profile->bio ?:Request::old('bio') }}">
		        @if ($errors->has('bio'))
					<div class="has-error-text error-input-profile">{{ $errors->first('bio') }}</div>
                 @endif
					</div>
					<div class="edit edit-links">
				     <label for="facebook_url" class="control-label">Facebook Link</label>
   		       <input type="text" name="facebook_url" class="form-control" id="facebook_url" value="{{ Auth::user()->profile->facebook_url?:Request::old('facebook_url') }}">
   		       @if ($errors->has('facebook_url'))
					<div class="has-error-text error-input-profile">{{ $errors->first('facebook_url') }}</div>
                 @endif
						 <br><br>
						 <label for="instagram_url" class="control-label">Instagram Link</label>
   		       <input type="text" name="instagram_url" class="form-control" id="instagram_url" value="{{ Auth::user()->profile->instagram_url?:Request::old('instagram_url') }}">
   		        @if ($errors->has('instagram_url'))
					<div class="has-error-text error-input-profile">{{ $errors->first('instagram_url') }}</div>
                 @endif
						 <br><br>
						 <label for="youtube_url" class="control-label">Youtube Link</label>
   		       <input type="text" name="youtube_url" class="form-control" id="youtube_url" value="{{ Auth::user()->profile->youtube_url?:Request::old('youtube_url') }}">
   		        @if ($errors->has('youtube_url'))
					<div class="has-error-text error-input-profile">{{ $errors->first('youtube_url') }}</div>
                 @endif
						 <br><br>
						 <label for="twitter_url" class="control-label">Twitter Link</label>
						 <input type="text" name="twitter_url" class="form-control" id="twitter_url" value="{{ Auth::user()->profile->twitter_url?:Request::old('twitter_url') }}">
						  @if ($errors->has('twitter_url'))
					<div class="has-error-text error-input-profile">{{ $errors->first('twitter_url') }}</div>
                 @endif
				  </div>
			 </div>

			<div class="form-group update-btn">
		        <button type="submit" class="btn btn-default">Update</button>
		  </div>
					<input type="hidden" name="_token" value="{{ Session::token() }}">
			</form>
		</div>
	</div>
<link href="{{ asset('/css/edit-profile.css') }}" rel="stylesheet">

@endsection
