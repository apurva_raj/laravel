@extends('layout.main')

@section('metatag')
    <title>{{ $title }}</title>
    <meta content="{{$title}}" property="og:title" />
    <meta content="{{$metaImg}}" property="og:image" />
@endsection

@section('content')
  <div class="container">
  <!-- Position for Left Side Bar -->
  <div class="cd1">
       	<div class="leftbar">
      		@include('layout.leftbar')
        </div>

        <!-- Position for Actual Content Area -->
      	<div class="inner-container">
      

          <div class = "post-feeds">
      			@include('post.post_ui')
      		</div>
        </div>
      </div>
  </div>
@endsection
