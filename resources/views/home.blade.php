@extends('layout.main')

@section('metatag')
    <title>{{ $title }}</title>
    <meta content="{{$title}}" property="og:title" />
    <meta content="{{$metaImg}}" property="og:image" />

@endsection


@section('tinymce')

<script type="text/javascript" src="{{ asset('/js/tinymce/tinymce.min.js') }}"></script>

<script type="text/javascript">
window.onload=function(){
tinyMCE.init({
  selector: "#postitle",
  menubar: false,
  toolbar: false,
  statusbar: false,
  plugins: "paste",
  paste_as_text: true,
  setup: function(ed){
    ed.on('keyup',function(e){
      var cnt=countCharsTitle();
      document.getElementById("titleLen").innerHTML=(150-cnt);
    });
  }
});
};
</script>
<script type="text/javascript">
window.onload=function(){
tinyMCE.init({
  selector: "#post_story",
  menubar: false,
  toolbar: "bold italic underline",
  statusbar: false,
  plugins: "paste autoresize",
  autoresize_bottom_margin: 7,
  editor_selector : "mceEditor",
  paste_as_text: true,

 setup: function(ed){
    ed.on('keyup',function(e){
      var count=countChars();
      document.getElementById("len").innerHTML=(2000-count);
    });
  }
});
};

function countChars() {
  var body = tinymce.get("post_story").getBody();
  var content = tinymce.trim(body.textContent);
  var len = $.trim(content).length;
  if(content.length>2000){
    document.getElementById("post_story_ifr").style.border="1px solid #b43636";
  }
  if(content.length<=2000){
    document.getElementById("post_story_ifr").style.border="none";
  }
  return len;
};
function countCharsTitle() {
  var b = tinymce.get("#post_title").getBody();
  var cntnt = tinymce.trim(b.innerText||b.textContent);
  var l = $.trim(cntnt.replace(/(<([^>]+)>)/ig, "")).length;
  if(cntnt.length>5){
    document.getElementById("post_title").style.border="1px solid #b43636";
  }
  if(cntnt.length<=5){
    document.getElementById("post_title").style.border="none";
  }
  return l;
};

</script>

@endsection
  @section('content')
  <div class="container ec1">
  <!-- Position for Left Side Bar -->
    <div class="cd1">
     	<div class="leftbar">
    		@include('layout.leftbar')
      </div>

      <!-- Position for Actual Content Area -->
    	<div class="inner-container">
        <div class = "post-input">
    			@include('post.update_post')
    	  </div>

        <div class = "post-feeds">
          <div class="scroll">
    			@include('post.post_ui')
          </div>
    		</div>
      </div>
    </div>
  </div>
@endsection
