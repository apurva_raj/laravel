
@extends('layout.main')
@section('metatag')

@endsection

@section('content')


<style media="screen">
	.top-right{
		margin-top: -52px !important;
	}
</style>
<div class="container" style="margin-top: 78px;">
  <div class="main">
      <div class="tags_to_follow">
					<ul>
						@foreach($topics as $topic)
						<a href="{{ route('topic_name', $topic->topic_slug_name) }}"><li class="tag tag_e">{{ $topic->topic_name }}</li></a>
						@endforeach
					</ul>
      </div>

			<div class="people_to_follow">

				<ul>

					@foreach($users as $user)
					<li class="people_e">
						<img src="{{ url($user->profile->display_pic) }}" alt="" class="img_e">
						<ul style="margin-left:-40px">
							<li class="pe1" style="font-size:17px;"><a href="{{ route('profile', $user->username) }}">{{ $user->name }} </a><br>
							<li class="pe1 pe2"><a href="#">{{ $user->post->count() }} Pictales</a></li>
							<li class="pe1 pe2"><a href="#">{{ $user->myfollowers()->count() }} followers</a></li>
						</ul>

				</li>
					@endforeach
				</ul>

				</ul>
			</div>

			<div class="pictales_to_read">

						@foreach($posts as $post)
			 				@include('partials.post-draft')
			 		 @endforeach

			</div>
  </div>
</div>

<style media="screen">

	.people_e{
		width: 237px;
	}
	.pe1{

		padding: 5px;
		font-size: 13px;
		width: 123px;
	}
	.pe2{
		margin-left: 5px;
	}
	.pe1 a{
		color: #656464 !important;

	}
	.tags_to_follow{
		margin-left: -5px;
	}
 .tags_to_follow ul li {
	 display: inline-block;
	 list-style: none;
 }
 .tag_e{
	 margin: 5px !important;
	 font-size: 15px !important;
 }
 .people_to_follow {
	 margin-top: 23px;

 }

 ul li {
	 list-style: none;
	 display: inline-flex;
 }

 .img_e{
	 width: 100px;
 	height: 100px;
 }

 .pictales_to_read{
	 -webkit-padding-start: 40px;
	 margin-top: -23px;
 }
 .tags-container {
	 height: 37px;
 }
</style>
