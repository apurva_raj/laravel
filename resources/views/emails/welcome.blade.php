<div class="body" style="font-size:1.3em;">
<div class= "invite-bg" style="background-color:#eee; display:block; padding-bottom:20px;">
	<div class="center" style="display:block; margin:0 auto; padding-top: 20px; width:100px;">
		<img class= "logo" style="width:100px;" src="http://oi67.tinypic.com/15nwtoh.jpg" alt="Pictales" />
		<br>
	</div>
	<div class="content" style=" margin:20px 70px; min-width: 400px; padding:5px; background-color:#fff;">
	<h3>Hii {{$name}}, welcome to Pictales</h3>
	<h2>Our vision is to make significant changes on how we read,experience and create quality and creative stories with time saving way. </h2>
  	<h2>Thank you for becoming a part of this vision.</h2>
  	<div>You can login to your Pictales account by clicking the Login button below or by visting
    <a href="http://www/pictales.me" style="text-decoration:none">Pictales.me</a>
    and using the username and password mentioned below:
  </div>
<br>
  <div>E-mail: <a href="mailto:{{$email}}">{{$email}}</a></div>
<br>
  <span>Password: Your password is the password you created when you registered on the site. If you forgot your password,
    you may recover it here: 	<a href="http://pictales.me/password/email/">http://pictales.me/password/email/</a></div>
  <br>
	<div class="join-btn" style="background-color:#ddd; border-radius:5px; display:block; margin:20px auto; max-width:150px; text-align:center;">
		<a href="http://www.pictales.me/" style="text-decoration:none; color:#000; display:block; padding:10px;">
			Login
		</a>
	</div>
		<div class="link" style="text-align:center; font-size:0.8em;">
			<span>Or you may copy/paste this link into your browser:</span>
		<br>
		<a href="http://www.pictales.me">http://pictales.me/</a>
		<br><br>This email was sent to you by  <a href="http://www/pictales.me" style="text-decoration:none">Pictales.me</a>
	</div>

	<p>You can reply this email to reach out to us anytime.</p>

</div>
</div>
</div>
</div>
