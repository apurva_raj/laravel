<div class="body" style="font-size:1.3em;">
<div class= "invite-bg" style="background-color:#eee; display:block; padding-bottom:20px;">
	<div class="center" style="display:block; padding-top: 15px; width:200px; padding-bottom: 15px; margin:0 auto;">
		<a href="www.pictales.me">
			<img class= "logo" style="width:200px; cursor:default;" src="http://oi67.tinypic.com/15nwtoh.jpg" alt="Pictales" />
		</a>
		<br>
	</div>
	<div class="content" style=" margin:0px 30px; padding:10px; background-color:#fff;">
	<h4>Hello ! <br>You are invited to Pictales by {{ $user }}</h4>

	<div class="join-btn" style="background-color:#2ab27b; border-radius:5px; display:block; color:#fff; margin:20px auto; max-width:150px; text-align:center;">
		<a href="http://www.pictales.me/mailer/register?code={{$code}}" style="text-decoration:none; color:#000; display:block; padding:10px;">
			<strong>Join Pictales</strong>
		</a>
	</div>
	<div class="link" style="text-align:center; font-size:0.8em;">
		<span>You may copy/paste this link into your browser:<a href="http://www.pictales.me/mailer/register?code={{$code}}">http://www.pictales.me/mailer/register?code={{$code}}</a></span>
		<br>
	</div>
	<br>
	<div class="email" style="text-align:center;">
	<span>Your sign in email is:</span>
	<br> {{ $email }}
</div>
</div>
</div>
</div>
