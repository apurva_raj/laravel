<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">

		@yield('metatag')
		<meta property="og:image:width" content="600" />
		<meta property="og:image:height" content="400" />

		@yield('css')

		<link href="{{ asset('css/stylesheet.css') }}" rel="stylesheet">
		<link href="{{ asset('css/profile.css') }}" rel="stylesheet">

		<script src="{{ asset('js/jquery.min.js') }}" type="text/javascript"></script>
		<script src="{{ asset('js/fixed-header.js') }}" type="text/javascript"></script>
		<script type="text/javascript" src="{{ asset('/js/bootstrap.min.js') }}"></script>
		<script type="text/javascript" src="{{ asset('/js/jquery.easyeditor.js') }}"></script>
		<script src="{{ asset('/js/jquery.leanModal.min.js') }}" type="text/javascript"></script>
		<script src="{{ asset('/js/clipboard.min.js') }}" type="text/javascript"></script>
		<script src="{{ asset('/js/readmore.min.js') }}" type="text/javascript"></script>

		<link href="https://fonts.googleapis.com/icon?family=Material+Icons"
      rel="stylesheet">
		@yield('tinymce')


		<script type="text/javascript">
			var pathtoGif = "{{ asset('/images/103.gif') }}";
			var APP_URL = {!! json_encode(url('/')) !!};
		</script>
		<script type="text/javascript" src="{{ asset('/js/jscroll/jquery.jscroll.min.js') }}"></script>
		<script src="{{ asset('/js/dropdown.js') }}" type="text/javascript"></script>
		<script>
			  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
			  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
			  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
			  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

			  ga('create', 'UA-49018061-2', 'auto');
			  ga('send', 'pageview');
		</script>
		<meta name="p:domain_verify" content="bdd814f59a40d5342f4e8ce39f861d5c" />

	<link href='https://fonts.googleapis.com/css?family=Roboto' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.4/css/selectize.default.min.css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.4/js/standalone/selectize.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js">

</script>

	</head>
@yield('public-post')
