<div class="topbar-container">
	<!--Content Visible only to Autheticated Users -->
		<div class="navigation">
			<div class="navbar">

				<div class="explore">
					<a href="{{ route('explore') }}"><i class="material-icons" style="font-size:36px;color:#656464;">explore</i></a>
				</div>
			<!--Website Logo Placeholder-->
					<div class="logo">
						<a href="{{ URL::route('home') }}"><span><img src="{{ asset('images/pictales-logo-by-hetu_archi.png') }}"></span></a>
					</div>

				<!--User Dropdown Options Placeholder-->
				<div class="top-right">

				<div class="user-info">
					<div class="profile-menxu" id="dropdown-menu">
						<img src="{{ url(Auth::user()->profile->display_thumb_pic) }}">
					</div>
					<div class="dropdown">


						<ul>
	  					<li><a class="drop" href=" {{ route('profile', Auth::user()->username) }}">Profile</a></li>
	  					<a class="drop" href="{{ route('mailer.invite') }}"><li>Invite</li></a>
	  					<a class="drop" href="{{ route('about') }}"><li>About</li></a>
	  					<a class="drop" href="{{ url('auth/logout') }}"><li>Logout</li></a>
	  					</ul>
	  					<div class="popover-arrow" style="margin-left: 8px;
    margin-top: -18px;"></div>
					</div>


				</div>

				<!-- Nofitications Dropdown Placeholder (Place after user dropdown as we are using "Float = Right") -->
				<div class="notify">
					<div class="notify-menu" id="notify-menu">
						<a><i class="material-icons" style="color: #575757;
">notifications</i>
</a>
					</div>
					<div class="notify-dropdown">
						<ul>
							@if($notify->count() > 0)
								@foreach($notify as $notification)
									<li>
										<a href="{{ $notification['attributes']['url'] }} ">{{ $notification['attributes']['text'] }}</a>
									</li>
								@endforeach
							@else
		          				<li>
									<span class="no-results">Nothing to show.</span>
		          				</li>
		          			@endif
		  				</ul>
						<div class="fixed-bottom">
							<div><a href="{{ route('notify.view') }}">See All</a></div>
						</div>
					</div>
					@if($notify->count() > 0)
						<div class="notify-label">{{ $notify->count() }}</div>
					@elseif($notify->count() > 99)
						<div class="notify-label">99</div>
					@endif
				</div>

			</div>




			</div>
		</div>

	<!--Flash Placeholder -->
	<div class="flashholder">
	@include('layout.flash')
	</div>
</div>

<!-- <div class="navspacer">
</div> -->
