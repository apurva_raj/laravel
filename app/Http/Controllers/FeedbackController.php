<?php

namespace App\Http\Controllers;
use Auth;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Feedback;
use App\Models\FeedbackVotes;
use Illuminate\Support\Facades\Redirect;


class FeedbackController extends Controller {

      public function main(){
        $notify = Auth::user()->getNotificationsNotRead(null,null,'desc');


        $feedback =  Feedback::orderBy('id', 'desc')->get();


        return view('features.feedback')
               ->with('feedbacks',$feedback)
               ->with('notify', $notify)
               ->with('title', 'Coming up')
               ->with('metaImg', 'N/A');
      }

      public function postComment(Request $request){
            $this->validate($request, [
                "comment_aws" => 'required',
            ]);

          $feedback= Auth::user()->feedback()->create([
            'comment' => $request->input('comment_aws'),
          ]);


          return back();
      }

      public function getDeleteComment($feedbackId){
        $feedback = Feedback::where('id', '=', $feedbackId)->first();
        if($feedback->user_id == Auth::user()->id){
          Feedback::where('id', $feedbackId)->delete();
        }
        return back();

      }

      public function getVote($feedbackId){
        $feedback = Feedback::where('id', '=', $feedbackId)->first();
         if(!$feedback->user_id == Auth::user()->id){
           $vote = FeedbackVotes::create([
             "user_id" => Auth::user()->id,
             "comment_id" => $feedbackId,
           ]);
         }
         return ;


      }

      public function getUnvote($feedbackId){
        $feedback = Feedback::find($feedbackId);
          if($feedback){
            FeedbackVotes::where('comment_id', $feedbackId)->delete();
          }
          return back();

      }
}
