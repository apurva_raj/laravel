<?php

namespace App\Http\Controllers;

use App\Models\Invite;
use Mail;
use Illuminate\Http\Request;
use Auth;
use Redirect;
use App\Models\RequestInvite;
use Session;
use Notifynder;


class InviteController extends Controller{
	public function invite(Request $request){
		if (Auth::user()->admin == "true"){
				$count = "Unlimited";
			}
		elseif (Auth::check()){
			$count = 3-Invite::where('user_id', '=', Auth::user()->id)->count();
			if (Auth::user()->admin == "true"){
				$count = "Unlimited";
			}
		}
		$notify = Auth::user()->getNotificationsNotRead(null,null,'desc');
		return view('auth.invite')
			   ->with('count', $count)
			   ->with('notify', $notify)
			   ->with('title', 'Invites')
			   ->with('metaImg', 'N/A');
	}


	public function postinvite(Request $request){

		if(Auth::user()->admin == "true"){
			$user = Auth::user()->name;
			$this->validate($request, [ 'email' => 'required|email|unique:invites|unique:users', ]);
			$data = array( 'email' =>$request['email'] , 'first_name' => $request['email'], $user);
			Mail::send('layout.mail', array('code'=> md5($request['email']), 'email'=> $request['email'], 'user'=>$user), function($message)use($data){
					$message->to($data['email'])->subject('Welcome to the Pictales!');
				});
			Auth::user()->invite()->create([
					'email' => $request['email'],
					'code' => md5($request['email']),
						'status' => "Pending",
			]);
			return Redirect::back()->with('info','Your invite is sent!');
		}
		else if (Auth::check()){
			if(Auth::user()->admin == "true")
			{ 		$user = Auth::user()->name;

				$this->validate($request, [ 'email' => 'required|email|unique:invites|unique:users', ]);

						$data = array( 'email' =>$request['email'] , 'first_name' => $request['email'], $user);
							Mail::send('layout.mail', array('code'=> md5($request['email']), 'email'=> $request['email'], 'user'=>$user), function($message)use($data){
							$message->to($data['email'])->subject('Welcome to the Pictales!');
					});
						Auth::user()->invite()->create([
										'email' => $request['email'],
										'code' => md5($request['email']),
										'status' => "Pending",
							]);
				return Redirect::back()->with('info','Your invite is sent!');
			}
			elseif(Invite::where('user_id', '=', Auth::user()->id)->count() < 3)
			{ 		$user = Auth::user()->name;

				$this->validate($request, [ 'email' => 'required|unique:invites|unique:users|unique:requests', ]);

	        	$data = array( 'email' =>$request['email'] , 'first_name' => $request['email'], $user);
	            Mail::send('layout.mail', array('code'=> md5($request['email']), 'email'=> $request['email'], 'user'=>$user), function($message)use($data){
        			$message->to($data['email'])->subject('Welcome to the Pictales!');
    			});
						Auth::user()->invite()->create([
							    	'email' => $request['email'],
										'code' => md5($request['email']),
										'status' => "Pending",
	            ]);
				return Redirect::back()->with('info','Your invite is sent!');
			}
			else
			{
				return Redirect::back()->with('info','You are allowed only 3 invites.');
			}
		}
	}


	public function requestinvite(Request $request){

		$this->validate($request, [ 'email' => 'required|unique:invites|unique:users|unique:requests', ]);
		RequestInvite::create([
	      'email' => $request['email'],
				'code' => md5($request['email']),
				'status' => "Pending",
	            ]);
				$data = array( 'email' =>$request['email'] , 'first_name' => $request['email']);
	    Mail::send('emails.request', array('email'=> $request['email']), function($message)use($data){
        	$message->to($data['email'])->subject('Welcome to the Pictales!');
    		});
		return Redirect::back()->with('info','Thanks for Requesting!');

	}

	public function authinvite(Request $request){
		if(Invite::where('code', '=', $request['code'])->count() == 1){
			Session::put('mode','Register');
			//$invite = Invite::where('code', '=', $request['code'])->first()->email;
			return redirect('auth/register?code='.$request['code'])->with('info','Please Register your account' );
		}
		elseif(RequestInvite::where('code', '=', $request['code'])->count() == 1){
			Session::put('mode','Register');
			//$invite = RequestInvite::where('code', '=', $request['code'])->first()->email;
			return redirect('auth/register?code='.$request['code'])->with('info','Please Register your account' );
		}
		else {
			return redirect('/')->with('info','Invalid Invite Code. Please Request new Invite' );}
	}

}
