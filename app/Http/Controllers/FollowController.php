<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Follow;
use App\Models\Notify;

use Auth;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Notifynder;

class FollowController extends Controller {

	public function followers($username) {

	    $user = User::where('username', $username)->first();
			if(!$user) {
				abort(400);
 	    }
			$notify = Auth::user()->getNotificationsNotRead(null,null,'desc');
	     	$title = $user->name;
		    $metaImg = "http://pictales.me".$user->profile->display_pic;

	     	 return view('follow.followers')
	     	 		->with('user', $user)
	     	 		->with('notify', $notify)
	     	 		->with('title', $title)
             		->with('metaImg', $metaImg);
			}

   public function following($username) {

	    $user = User::where('username', $username)->first();
	    if(!$user) {
	    	 abort(404);
	    }
			$notify = Auth::user()->getNotificationsNotRead(null,null,'desc');
	      	$title = $user->name;
      	    $metaImg = "http://pictales.me".$user->profile->display_pic;


	      return view('follow.following')
	      		->with('user', $user)
	      		->with('notify', $notify)
	      		->with('title', $title)
             	->with('metaImg', $metaImg);
   }

   public function follow($username) {

		 $follower_id = Auth::user()->id;
 		 $following_to_id = User::where('username', $username)->first()->id;

 		 if($following_to_id) {
 		 	Auth::user()->follow()->create([
  			'follower_id' =>$follower_id,
				'followee_id' =>$following_to_id,
  		]);
 		 Notifynder::category('user.following')
            ->from($follower_id)
            ->to($following_to_id)
            ->url(url()."/".Auth::user()->username)
            ->send();
	    }

   }

	 public function unfollow($username){
		 $user = User::where('username', $username)->first()->id;
		  $user_from = Auth::user()->id;
		  $fields = ['from_id' => Auth::user()->id, 'to_id' => $user, 'category_id' => '1'];
		  $notify_id=Notify::where($fields)->first()->id;
		 Notifynder::delete($notify_id);

		 $fields=['follower_id'=>Auth::user()->id, 'followee_id'=>$user];
		 Follow::where($fields)->delete();

	 }


	
}
