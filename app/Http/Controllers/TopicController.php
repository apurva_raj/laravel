<?php

namespace App\Http\Controllers;
use App\Models\Topic;
use Illuminate\Http\Request;
use App\Models\Post;
use App\Models\User;
use App\Models\Topicfollow;

use Auth;
use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class TopicController extends Controller
{

      public function main($topic_name){

          $topic = Topic::where('topic_slug_name', '=', $topic_name)->first();

          if(!Auth::user()){
            $posts = $topic->post()->orderBy('created_at', 'desc')->get();

            return view('topics.main')
                  ->with('posts', $posts)
                   ->with('topic', $topic)
                   ->with('topicfollow')
                   ->with('notify', "no")
                   ->with('comments')
                   ->with('likes')
                   ->with('user')
                   ->with('readmore', 'sc1');
        } else {
          $user = Auth::user();
          $notify = Auth::user()->getNotificationsNotRead(null,null,'desc');

          $posts = $topic->post()->orderBy('created_at', 'desc')->get();

          return view('topics.main')
                ->with('posts', $posts)
                 ->with('topic', $topic)
                 ->with('topicfollow')
                 ->with('user', $user)
                 ->with('notify', $notify)
                 ->with('comments')
                 ->with('likes')
                 ->with('readmore', 'sc1');

        }
    }


    //topic follow
    public function tfollow($id) {

     $follower_id = Auth::user()->id;
     if($id) {
      Auth::user()->followstopic()->create([
        'follower_id' =>$follower_id,
        'topic_id' =>$id,
      ]);

      }
    }

    //topic unfollow

    public function tunfollow($id){
     $follower_id = Auth::user()->id;
     $fields=['follower_id'=>$follower_id, 'topic_id'=>$id];
     Topicfollow::where($fields)->delete();

    }


    public function topic_tag(){
      $post = Post::find(1);
      $post->topics()->attach([1,3,2]);
      $post->topics()->sync([1,3,2]);

    }



    //topic search

    public function Tsearch(Request $request)
    {
        // Gets the query string from our form submission
        $topic = $request->input('search');
        // Returns an array of articles that have the query string located somewhere within
        // our articles titles. Paginates them so we can break up lots of search results.
        $topics = DB::table('topics')
                      ->where('topic_name', 'LIKE', '%' . $topic . '%')
                      ->orderBy('topic_name')
                      ->paginate(3);


        // returns a view and passes the view the list of articles and the original query.
        return view('layout.searchTopic', compact('topics', 'query'));
     }


}
