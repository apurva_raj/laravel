<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Auth;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Notifynder;
use \Input as Input;
use Intervention\Image\Facades\Image;


class ProfileController extends Controller {


     public function main($username) {

         $user = User::where('username', $username)->first();
         if(!$user) {
            abort(404);
         }
         if(Auth::user()){
            $notify = Auth::user()->getNotificationsNotRead(null,null,'desc');
            $posts = $user->post()->orderBy('created_at', 'desc')->get();
            $title = $user->name;
            $ds = $user->profile->bio;
            $metaImg = "http://pictales.me".$user->profile->display_pic;

              return view('profile.main')
                     ->with('user', $user)
                     ->with('posts', $posts)
                     ->with('notify', $notify)
                     ->with('title', $title)
                     ->with('ds', $ds)
                     ->with('metaImg', $metaImg)
                     ->with('readmore', 'sc1');
         } else {
           $posts = $user->post()->orderBy('created_at', 'desc')->get();
           $title = $user->name;
           $ds = $user->profile->bio;
           $metaImg = "http://pictales.me".$user->profile->display_pic;

             return view('profile.main')
                    ->with('user', $user)
                    ->with('posts', $posts)
                    ->with('title', $title)
                    ->with('ds', $ds)
                    ->with('metaImg', $metaImg)
                    ->with('readmore', 'sc1');

         }
}

  public function getEdit(){
    $notify = Auth::user()->getNotificationsNotRead(null,null,'desc');
  		return view('profile.edit')->with('notify',$notify)->with('title', 'Edit profile')->with('metaImg', 'N/A');
  	}


  	public function postEdit(Request $request){
  		$this ->validate($request, [
  			'name' => 'required|min:6|max:24',
  			'status' => 'max:150',
        'bio' => 'max:1042',
  			'facebook_url' => 'url',
  			'instagram_url' => 'url',
  			'youtube_url' => 'url',
			  'twitter_url' => 'url',
        'file_img'=> 'mimes:jpeg,jpg,bmp,png',
  		]);

      $fileE= Input::hasFile('file_img');
        if($fileE){
          $file = Input::file('file_img');
          $publicPath = public_path();

          $tmpFilePath = '/uploads/dp/';
          $tmpFileName = time() . '-' . $file->getClientOriginalName();
          $file = $file->move(public_path() . $tmpFilePath, $tmpFileName);
          $path = $tmpFilePath . $tmpFileName;

          $thumb = Image::make('uploads/dp/'.$tmpFileName)->resize(60,60)->save('uploads/dp/thumbs/'.$tmpFileName, 60);
          $path_thumb = $tmpFilePath.'thumbs/'. $tmpFileName;

          Auth::user()->Profile->update([
              'display_pic' =>$path,
              'display_thumb_pic' => $path_thumb,
            ]);
        }
  		Auth::user()->update([
  			'name' =>$request->input('name'),
  			'status' =>$request->input('status'),

  		]);
  		Auth::user()->profile->update([
  			'status' =>$request->input('status'),
        'bio' =>$request->input('bio'),
  			'facebook_url' =>$request->input('facebook_url'),
  			'instagram_url' =>$request->input('instagram_url'),
  			'youtube_url' =>$request->input('youtube_url'),
  			'twitter_url' =>$request->input('twitter_url'),
  		]);
  		return redirect()->route('profile',Auth::user()->username)->with('info','Profile Updated');;
  	}

}
