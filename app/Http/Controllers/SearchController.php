<?php

namespace App\Http\Controllers;

use Input;
use App\Models\User;
use Illuminate\Http\Request;

use DB;

use App\Http\Controllers\Controller;

class SearchController extends Controller
{

	 public function search(){
		 // Check if user has sent a search query
		if($query = Input::get('query', false)) {
			// Use the Elasticquent search method to search ElasticSearch
			$users = User::searchByQuery(array('match' => array('name' => $query)));
			return view('layout.search')->with('users', $users);
		}

		else {
			// Show all posts if no query is set
			$users = User::all();
			return view('layout.search')->with('users', $users);
		}
	}


	public function putMapping(){
		User::putMapping();
		return view('layout.search');
	}

	public function createIndex(){
		User::createIndex();
		return view('layout.search');
	}

	public function putAnalyzer(){
		User::putAnalyzer();
		return view('layout.search');
	}

	public function addtoIndex(){
		User::addAllToIndex();
		return view('layout.search');
	}
}
