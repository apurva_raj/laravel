<?php namespace App\Http\Controllers;

  use Illuminate\Http\Request;
  use App\Models\User;
  use Mail;
  use Auth;
  use App\Http\Requests;
  use App\Http\Controllers\Controller;
  use Notifynder;

  class AdminController extends Controller {

    public function view() {
      $notify = Auth::user()->getNotificationsNotRead(null,null,'desc');
      return view('admin.home')->with('notify', $notify);
    }
  }
