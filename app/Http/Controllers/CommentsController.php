<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Comment;
use App\Models\Like;

use App\Models\Post;

use Auth;
use Illuminate\Http\Request;


class CommentsController extends Controller
{
    public function postComment(Request $request, $postId){
      $this->validate($request, [
          "comment{$postId}" => 'required',

      ]);

      if(!$postId) {
        return false;
      }
      $user_id = Auth::user()->id;


      $comment = Comment::create([
         'user_id' => $user_id,
         'post_id' => $postId,
         'comment' => $request->input("comment{$postId}"),
      ]);

      return redirect()->back();
    }
    public function getLike($postId) {

      $post = Post::find($postId);

       if(!$postId) {
         return redirect()->route('home');
       }
        // if(Auth::user()->hasLiked($postId)){
        //   return redirect()->back();
        // }

       $like = $post->user->likes()->create([
         "post_id" => $postId,
       ]);
       Auth::user()->likes()->save($like);

       return redirect()->back();

    }

    public function getUnlike($postId) {
        Like::where('post_id',$postId)->where('post_id', $postId)->delete();
        return redirect()->back();

    }
}
