<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Comment;
use App\Models\Like;
use App\Events;

use App\Models\Post;
use View;
use Auth;
use Illuminate\Http\Request;
use Notifynder;


class LikesAndCommentsController extends Controller
{
    public function postComment(Request $request){
      $this->validate($request, [
          "comment" => 'required',
      ]);

      if(!$request['postid']) {
        return false;
      }
      $userid = Auth::user()->id;
      $post = Post::find($request['postid']);

      $comment = Comment::create([
         'user_id' => $userid,
         'post_id' => $request['postid'],
         'comment' => $request['comment'],
      ]);
      if(Auth::user()->id != $post->user_id)
      {
        Notifynder::category('user.comments')
              ->from(Auth::user()->id)
              ->to($post->user_id)
              ->url(url()."/".$post->user->username."/".$post->slug_title)
              ->extra(array( 'title' => $post->title, 'commentid' => $comment->id))
              ->send();
      }
      // Old Refresh Method - return redirect()->back();
      $post = POST::where('id', $request['postid'])->first();
      return  view('partials.comment-draft')->with('post',$post)->with('comment', $comment);
    }

    public function getDeleteComment($commentId){
      $comment = Comment::where('id', '=', $commentId)->first();
      $postId = $comment->post_id;
      $post = Post::where('id','=', $postId)->first();
      if(Auth::user()->id != $post->user_id)
            {
            $fields = [
              'from_id' => Auth::user()->id,
              'category_id' => '3',
              'url' => url()."/".$post->user->username."/".$post->slug_title,
              'extra' => '{"title":"'.$post->title.'","commentid":'.$commentId.'}'];
              if($fields){
            $notify = Auth::user()->notify()->where($fields)->first()->id;
            Notifynder::delete($notify);
              }
          }
      if(Auth::user()->id == $comment->user->id) {
      Comment::where('id',$commentId)->delete();
     }
    }


    public function getLike($postId) {
      $user = Auth::user();

      $post = Post::find($postId);
       if(!$post) {
         return redirect()->route('home');
       }
       if($user->id != $post->user_id)
       {
       Notifynder::category('user.likes')
            ->from(Auth::user()->id)
            ->to($post->user_id)
            ->url(url()."/".$post->user->username."/".$post->slug_title)
            ->extra(array( 'title' => $post->title,
                            'postid' => $postId))
            ->send();
        }

        if($user->id != $post->user_id){
          $like = Like::create([
            "post_id" => $postId,
            "user_id" => Auth::user()->id,
          ]);

        }

    }

    public function getUnlike($postId) {
       $post = Post::find($postId);
       $user = Auth::user();
       if(Auth::user()->id != $post->user_id)
       {
         $fields = [
           'from_id' => Auth::user()->id,
           'category_id' => '2',
           'url' => url()."/".$post->user->username."/".$post->slug_title,
           'extra' => '{"title":"'.$post->title.'","postid":"'.$postId.'"}'];
           if($fields){
             $notify = Auth::user()->notify()->where($fields)->first()->id;
             Notifynder::delete($notify);
          }
       }
        $fields=['post_id' => $postId, 'user_id' => Auth::user()->id];
        Like::where($fields)->delete();
     }
}
