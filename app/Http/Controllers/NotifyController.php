<?php

namespace App\Http\Controllers;
use Notifynder;
use App\Models\User;
use App\Models\Post;
use App\Models\Notify as Notify;
use Auth;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class NotifyController extends Controller
{
  public function getnotify(Request $request) {
    //This function is unused i guess. Safe to Delete??
    $user= Auth::user();
    $notifications = Notifynder::getAll($user->id,null,false);
    $notify = Auth::user()->getNotificationsNotRead(null,null,'desc');
    return view('partials.notify-draft')->with('notify', $notify)->with('notifications', $notifications);
  }


	public function viewall() {
		$user = Auth::user();
	
    $noti = Notify::find($user->id);

		$notifications = Notifynder::getAll($user->id,null,false);
		$notify = Auth::user()->getNotificationsNotRead(null,null,'desc');

		return view('layout.notifications')
          ->with('notify', $notify)
          ->with('noti', $noti)
          ->with('notifications', $notifications)
          ->with('title', 'Notifications')
          ->with('metaImg', 'N/A');
	}

  public function readall() {

    Notifynder::ReadAll(Auth::user()->id);
  }
}
