<?php

namespace App\Http\Controllers\Auth;

use App\Models\User;
use App\Models\Profile;
use App\Models\Follow;
use App\Models\Notify;
use Validator;
use App;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Session;
use App\Models\Invite;
use App\Models\RequestInvite;
use Illuminate\Http\Redirect;
use App\Events\UserSignedUp;
use Mail;


class StartupController extends Controller
{

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;


    public function __construct()
    {
        $this->middleware('guest', ['except' => 'getLogout']);
    }


    protected function validator(array $data)
    {
        return Validator::make($data, [
            'email' => 'required|email|max:255|unique:users',
        ]);
    }



    protected function main(array $data){
      dd($data);
    }


    protected $redirectTo = 'home';

}
