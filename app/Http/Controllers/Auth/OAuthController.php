<?php

namespace App\Http\Controllers\Auth;

use Auth;
use Redirect;
use Laravel\Socialite\Contracts\Factory as Socialite;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\Invite;
use App\Models\Follow;
use App\Models\RequestInvite;
use Session;
use App\Models\Profile;
use App\Models\Notify;
use App\Events\UserSignedUp;
use Intervention\Image\Facades\Image;

 class OAuthController extends Controller
 {
       public function __construct(Socialite $socialite){
           $this->socialite = $socialite;
       }


       public function getLogin($provider=null)
       {
           if(!config("services.$provider")) abort('404');//just to handle providers that doesn't exist
           Session::put('mode', "Login");
           return $this->socialite->with($provider)->redirect();
       }


       public function getRegister($provider=null)
       {
          if(!config("services.$provider")) abort('404'); //just to handle providers that doesn't exist
          Session::put('mode', "Register");
          Session::put('provider', $provider);
          return $this->socialite->with($provider)->redirect();
        }


       public function getSocialAuthCallback($provider=null)
       {
          $mode=Session::pull('mode');
          if($oauth_user = $this->socialite->with($provider)->user())
          {
            if($mode=="Login")
            {
              return $authUser = $this->LoginUser($oauth_user);
            }
            else //For Register
            {
              if ($invite = Invite::where('email', '=',$oauth_user->getEmail())->first()) {
                if ($invite['status']=='Pending') {
                  $invite->update([
                    'status' =>'Claimed',
                  ]);
                  return $authUser = $this->RegisterUser($oauth_user);
                }
                else {
                  return Redirect::to('/')->with('info', 'This Invite Code is already used. Please request for a new invite code');
                }
              }
              elseif ($request = RequestInvite::where('email', '=',$oauth_user->getEmail())->first()) {
                if ($request['status']=='Pending') {
                  $request->update([
                    'status' =>'Claimed',
                  ]);
                  return $authUser = $this->RegisterUser($oauth_user);
                }
                else {
                  return Redirect::to('/')->with('info', 'This Invite Code is already used. Please request for a new invite code');
                }
              }
              else {
                return Redirect::to('/')->with('info', 'Invalid Invite Code');
              }
            }
          }
          else
          {
            return Redirect::to('/')->with('info', 'Unable to Login with '.$provider.'. Please try again later.');
          }
        }

        private function LoginUser($oauth_user)
        {
          if ($authUser = User::where('email', $oauth_user->getEmail())->first()) {
            Auth::login($authUser, true);
            return Redirect::to('/home');
          }
          else {
            return Redirect::to('/')->with('info', 'Please Request for an Invite to register to this site');
          }
        }

        private function RegisterUser($oauth_user)
        {
          if ($authUser = User::where('email', $oauth_user->getEmail())->first()) {
            Auth::login($authUser, true);
            return Redirect::to('/home');
          }
          else {
             $publicPath = public_path();
            $tmpFilePath = '/uploads/dp/';
            $tmpFileName = time() .'-'. $oauth_user->getId().".png";
            $downloadpath=$publicPath.$tmpFilePath . $tmpFileName;
            $path = $tmpFilePath . $tmpFileName;
            $provider = Session::pull('provider');
            if ($provider == "google"){
              $avatar = preg_replace('/\?sz=[\d]*$/', '', $oauth_user->getAvatar());
            }
            else if ($provider == "facebook"){
              $avatar = $oauth_user->avatar_original;
            }
            else {
              $avatar = $oauth_user->getAvatar();
            }
            $file = file_get_contents($avatar);
            $save = file_put_contents($downloadpath, $file);
            $thumb = Image::make('uploads/dp/'.$tmpFileName)->resize(60,60)->save('uploads/dp/thumbs/'.$tmpFileName, 60);
            $path_thumb = $tmpFilePath.'thumbs/'. $tmpFileName;
            $user=User::create([
            'name' => $oauth_user->getName(),
            'email' => $oauth_user->getEmail(),
            'password' => bcrypt("trial"),
            ]);
            $profile = new Profile([
            'status' => 'Edit your status',
            'display_pic' =>  $path,
            'display_thumb_pic' => $path_thumb,
            ]);
            $user->profile()->save($profile);

            $follow = new Follow ([
                'followee_id' =>"1",
            ]);

            $user->follow()->save($follow);

            $notify = new Notify ([
                'from_id' => $user['id'],
                'to_id' =>"1",
                'category_id' =>"1", 
                'url' => url()."/".$user['username'],
            ]);

            $user->notify()->save($notify);
            User::addAllToIndex();            
            //sending welcome email
           // Mail::send('emails.welcome', array('email'=> $oauth_user->getEmail(), 'name'=> $oauth_user->getName()), function($message){
           // $message->to($oauth_user->getEmail())->subject('Welcome to the Pictales!');
            Auth::login($user, true);
            return Redirect::to('home');

       // });

          }
        }


}
