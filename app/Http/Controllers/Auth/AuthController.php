<?php

namespace App\Http\Controllers\Auth;

use App\Models\User;
use App\Models\Profile;
use App\Models\Follow;
use App\Models\Notify;
use Validator;
use App;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Session;
use App\Models\Invite;
use App\Models\RequestInvite;
use Illuminate\Http\Redirect;
use App\Events\UserSignedUp;
use Mail;


class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'getLogout']);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|confirmed|min:6',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        $email = $data['email'];
        $user = User::create([
        'name' => $data['name'],
        'email' => $data['email'],
        'password' => bcrypt($data['password']),
        ]);


        $profile = new Profile([
            'display_pic' =>'/images/user-default.jpg',
            'display_thumb_pic' =>'/images/user-default-thumb.jpg',
            'status' => 'Edit your status'
        ]);

        $user->profile()->save($profile);

        $follow = new Follow ([
                'followee_id' =>"1",
        ]);

        $user->follow()->save($follow);

        $isfollow = new Follow ([
                'follower_id' =>"1",
        ]);

        $user->isfollow()->save($isfollow);

        $notify = new Notify ([
                'to_id' =>"1",
                'category_id' =>"1", 
                'url' => url()."/".$user['username'],
        ]);

        $user->notify()->save($notify);

        //sending welcome email
        $data = array('email'=> $data['email'], 'name'=> $data['name']);
        Mail::send('emails.welcome', array('email'=> $data['email'], 'name'=> $data['name']), function($message)use($data){
        $message->to($data['email'])->subject('Welcome to the Pictales!');
    });
        User::addAllToIndex();
        return $user;
    }

    protected $redirectTo = 'home';

}
