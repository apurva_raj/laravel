<?php namespace App\Http\Controllers;

  use Illuminate\Http\Request;
  use App\Models\Post;
  use App\Models\Surprise;
  use App\Models\User;
  use App\Models\Comment;
  use App\Models\Topic;
  use App\Models\Profile;

  use Mail;
  use Auth;
  use App\Http\Requests;
  use App\Http\Controllers\Controller;
  use Notifynder;

  class HomeController extends Controller {


    public function home() {
      if(Auth::guest()) {
        $user = 'guest';
        $title = 'Pictales';
        $metaImg = "http://pictales.me/images/pictales-logo-by-hetu_archi.png";

        $posts = Post::where(function($query){
           return $query->where('wall', '=', 'true');
         })->orderBy('created_at', 'desc')->paginate(6);
            return view('welcome')->with('posts', $posts)
                                      ->with('comments')
                                      ->with('likes')
                                      ->with('likes')
                                      ->with('user', $user)
                                      ->with('title', $title)
                                      ->with('metaImg',$metaImg)
                                      ->with('readmore', 'sc1');

      }
      else {
        if(Auth::user()) {
          return redirect('home');
        }
      }
    }

    public function test() {
     $user = 'guest';
     $title = 'guest';
     $metaImg = "http://pictales.me/images/pictales-logo-by-hetu_archi.png";

     $posts = Post::where(function($query){
        return $query->where('wall', '=', 'true');
        })->orderBy('created_at', 'desc')->paginate(10);
         return view('home.test')->with('posts', $posts)
                                   ->with('comments')
                                   ->with('likes')
                                   ->with('likes')
                                   ->with('user', $user)
                                   ->with('title', $title)
                                   ->with('metaImg',$metaImg)
                                   ->with('readmore', 'sc1');

      }

    public function index() {
     $user = Auth::user();

     $title = $user->name;
     $metaImg = "http://pictales.me/images/pictales-logo-by-hetu_archi.png";
     $notify = Auth::user()->getNotificationsNotRead(null,null,'desc');
     $posts = Post::where(function($query){
        return $query->where('user_id', Auth::user()->id)
                      ->orWhereIn('user_id', Auth::user()->ImFollwing()->lists('followee_id'));
        })->orderBy('created_at', 'desc')->paginate(5);
         return view('home')->with('posts', $posts)
                                   ->with('comments')
                                   ->with('likes')
                                   ->with('likes')
                                   ->with('user', $user)
                                   ->with('notify', $notify)
                                   ->with('title', $title)
                                   ->with('metaImg',$metaImg)
                                   ->with('readmore', 'sc1');

      }

     public function gift(){
     $user = Auth::user();
     $title = "Surprise Me";
     $metaImg = "http://pictales.me/images/pictales-logo-by-hetu_archi.png";
     $notify = Auth::user()->getNotificationsNotRead(null,null,'desc');
     $posts = Post::where(function($query){
        return $query->where('stared', '=', 'true');
      })->orderBy('created_at', 'desc')->paginate(5);
         return view('surprise.surprise')->with('posts', $posts)
                                   ->with('comments')
                                   ->with('likes')
                                   ->with('likes')
                                   ->with('user', $user)
                                   ->with('notify', $notify)
                                   ->with('title', $title)
                                   ->with('metaImg',$metaImg)
                                   ->with('readmore', 'sc1');

      }

      public function sl(){
        if(Auth::guest()) {
       $metaImg = "http://pictales.me/images/pictales-logo-by-hetu_archi.png";

        return view('auth.startup')->with('title', 'Pictales')->with('metaImg', $metaImg);
      }
      else {
        if(Auth::user()) {
          return redirect('home');
        }
      }
      }

      public function addpost() {
        $user = Auth::user();
        $title = "Add pictale";
        $notify = Auth::user()->getNotificationsNotRead(null,null,'desc');

            return view('layout.addpost')->with('user', $user)->with('notify', $notify)->with('title', $title)->with('metaImg', 'N/A');
    }

    public function mail() {
          return view('emails.request');
    }

    public function TnQ() {
        return view('home.terms');
    }

    public function about(){
      $title = 'Pictales';
      $metaImg = "http://pictales.me/images/pictales-logo-by-hetu_archi.png";
      return view('home.about')->with('title', $title)->with('metaImg', $metaImg);
    }
    public function explore(){
      if(Auth::user()) {
      $notify = Auth::user()->getNotificationsNotRead(null,null,'desc');
    } else {
      $notify = 'null'; }
      $topics = Topic::paginate(20);

      $users = User::paginate(8);

      $posts = Post::paginate(5);

      return view('explore.main')
                ->with('notify', $notify)
                ->with('topics', $topics)
                ->with('users', $users)
                ->with('posts', $posts)
                ->with('readmore', 'sc1');
;
      }



}
