<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Contracts\Filesystem\Filesystem;
use App\Models\User;
use Auth;
use Storage;
use File;
use App\Models\Post;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use \Input as Input;
use Notifynder;
use Intervention\Image\Facades\Image;




class PostPictalesController extends Controller {

	public function getPictales(Request $request) {


		return view('post.main');
	}

  	public function postPictalesAdd(Request $request) {
    		$this->validate($request,['post_story' => 'required|max:2000', 'file_img' => 'required', 'title' => 'required | max:155' ]);

				$file = Input::file('file_img');
				$file_name = $file->getClientOriginalName();
				$file_name = md5($file_name);

				//temp original file
				$publicPath = public_path();
        $tmpFilePath = '/uploads/';
        $tmpFileName = time() . '-' . $file_name;

				//moving it to temp location
        $file = $file->move($publicPath . $tmpFilePath, $tmpFileName);

				//Resizing the image and saving it to final location
				$thumb = Image::make('uploads/'.$tmpFileName)->resize(650,null, function($constraint){ $constraint->aspectRatio();})->save('uploads/posts/'.$tmpFileName);

				//Deleting the original file
				File::delete($file);

				//getting the path of file
				$path = '/uploads/posts/'. $tmpFileName;


				$share_md5 = md5($path);

				//adding values to database
		  	$post = Auth::user()->post()->create([
           'file_path' => $path,
           'post_story' =>$request->input('post_story'),
           'title' =>$request->input('title'),
					 'share_md5' => $share_md5,
            ]);

			// Old Refresh Method - return redirect()->back();
			return  view('partials.post-draft')->with('post',$post)->with('info','Posted')->with('readmore', 'na');


    }

	public function getPictalesdelete($postId){
			$post = Post::where('id', '=', $postId)->first();

			if(Auth::user()->id == $post->user->id) {

					Post::where('id', $postId)->delete();
					return redirect()->back();
			}
	}

		public function getPictalesPublicShareLink($public_share_link) {
				return true;
		}

		public function getShareLink($share_md5) {

 				$post = Post::where('share_md5', '=', $share_md5)->firstOrFail();
				$user = $post->user;
				$title = $post->title." | ".$user->name;
				$ds = $post->post_story;
				$ds = substr($ds,0,275);
				$ds = str_replace(array("\n", "\t", "\r"), '', $ds);
				$ds = strip_tags($ds);

			    $metaImg = "http://pictales.me".$post->file_path;

				if(Auth::user()){
					$notify = Auth::user()->getNotificationsNotRead(null,null,'desc');
					return view('post.public_single_post')->with('post', $post)->with('notify', $notify)->with('title', $title)->with('metaImg', $metaImg)->with('ds',$ds)->with('readmore', 'na');
				} else {
					return view('post.public_single_post')->with('post', $post)->with('title', $title)->with('metaImg', $metaImg)->with('ds',$ds)->with('readmore', 'na');
				}
		}

	
	public function getPictalesCommonLink($username, $postSlugTitle){
		$user = User::where('username', $username)->first();
	}



  public function getPictalesName($username , $filename){
  	$user = User::where('username','=',$username)->first();
  	$post = Post::where('slug_title', '=', $filename)->firstOrFail();
			if(Auth::check()){
				if($user && $post && $user->id == $post->user->id) {
					$post = Post::where('slug_title', '=', $filename)->firstOrFail();
					$user = $post->user;
					$notify = Auth::user()->getNotificationsNotRead(null,null,'desc');
					$title = $post->title." by ".$user->name;
				  $metaImg = "http://pictales.me".$post->file_path;

				   	return view('post.single_post')
				   				->with('post', $post)
				   				->with('user',$user)
				   				->with('notify',$notify)
				   				->with('title', $title)
				   				->with('metaImg', $metaImg)
									->with('readmore', 'na');
 	    		}
 	    		else
 	    		{
 	    			abort(404);
 	    		}

				} else {
					if($user && $post && $user->id == $post->user->id) {
						$post = Post::where('slug_title', '=', $filename)->firstOrFail();
						$user = $post->user;
						$title = $post->title." by ".$user->name;
					  $metaImg = "http://pictales.me".$post->file_path;

					   	return view('post.single_post')
					   				->with('post', $post)
					   				->with('user',$user)
					   				->with('title', $title)
					   				->with('metaImg', $metaImg)
										->with('readmore', 'na');

	 	    		}
	 	    		else
	 	    		{
	 	    			abort(404);
	 	    		}
				}

  }

  public function postComment(Request $request, $postId) {
    $this->validate($request, [
       "comment{$postId}"  => 'required'
     ]);

  }
}
