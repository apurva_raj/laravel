<?php

use App\Models\User;

/*
Home
*/
Route::get('',['middleware' => ['guest'],'as' => 'welcome', 'uses' => 'HomeController@home',]);
Route::get('home',['middleware' => ['auth'],'as' => 'home', 'uses' => 'HomeController@index',]);
Route::get('mail',['middleware' => ['auth'],'as' => 'mail', 'uses' => 'HomeController@mail',]);
Route::get('send',['middleware' => ['auth'],'as' => 'send', 'uses' => 'HomeController@send',]);
Route::get('addpost',['middleware' => ['auth'],'as' => 'addpost', 'uses' => 'HomeController@addpost',]);
Route::controllers(['auth' => 'Auth\AuthController','password' => 'Auth\PasswordController',]);

Route::get('SurpriseMe',['middleware' => ['auth'],'as' => 'surprise', 'uses' => 'HomeController@gift',]);
Route::get('StartupLogin',['middleware' => ['guest'],'as' => 'StartupLogin', 'uses' => 'HomeController@sl',]);


Route::get('test',['middleware' => ['guest'],'as' => 'test', 'uses' => 'HomeController@test',]);
Route::get('about',['as' => 'about', 'uses' => 'HomeController@about',]);
Route::get('TnQ',['as' => 'TnQ', 'uses' => 'HomeController@terms',]);

// For the Topic
Route::get('topics/{topic_name}', ['as' => 'topic_name','uses' => 'TopicController@main',]);
Route::get('topics/test/list', ['as' => 'topic_name_test','uses' => 'TopicController@topic_tag',]);

//Explore

Route::get('explore', ['as' => 'explore', 'uses'=> 'HomeController@explore']);

/**
*
*Social Login
*
**/
Route::get('/login/{provider?}',['middleware' => ['guest'],'uses' => 'Auth\OAuthController@getLogin','as' => 'oauth.getSocialAuth']);
Route::get('/login/callback/{provider?}',['middleware' => ['guest'],'uses' => 'Auth\OAuthController@getSocialAuthCallback','as' => 'oauth.getSocialAuthCallback']);
Route::get('/register/{provider?}',['middleware' => ['guest'],'uses' => 'Auth\OAuthController@getRegister','as' => 'oauth.getSocialAuth']);
/**
*
* Invites
*
**/
Route::post('/mailer/invite',['middleware' => ['auth'],'uses' => 'InviteController@postinvite',]);
Route::get('/mailer/invite',['middleware' => ['auth'],'as' => 'mailer.invite','uses' => 'InviteController@invite',]);
Route::get('/mailer/register',['middleware' => ['guest'],'uses' => 'InviteController@authinvite',]);
Route::post('/request',['middleware' => ['guest'],'uses' => 'InviteController@requestinvite',]);
/**
*
*USER PROFILE
*
**/
Route::get('/{username}', ['as' => 'profile','uses' => 'ProfileController@main',]);
Route::get('/profile/edit',['middleware' => ['auth'],'as' => 'profile.edit','uses' => 'ProfileController@getEdit',]);
Route::post('/profile/edit',['middleware' => ['auth'],'uses' => 'ProfileController@postEdit',]);

/**
*
* Follow and following
*
**/
Route::get('notify/viewall', ['as' => 'notify.view','uses' => 'NotifyController@viewall',]);
Route::get('{username}/followers',['middleware' => ['auth'],'as' => 'followers','uses' => 'FollowController@followers',]);
Route::get('{username}/following', ['middleware' => ['auth'],'as' => 'following','uses' => 'FollowController@following',]);
Route::get('{username}/follow',['middleware' => ['auth'],'as' => 'follow','uses' => 'FollowController@follow',]);
Route::get('{username}/unfollow',['middleware' => ['auth'],'as' => 'unfollow','uses' => 'FollowController@unfollow',]);
/**
*
* Folllow TOPICS
*
**/
Route::get('/topic/{id}/follow',['middleware' => ['auth'],'as' => 'follow_topic','uses' => 'TopicController@tfollow',]);
Route::get('/topic/{id}/unfollow',['middleware' => ['auth'],'as' => 'unfollow_topic','uses' => 'TopicController@tunfollow',]);



/**
*
* Post Pictales
*
**/

Route::get('/post',['middleware' => ['auth'],'as' => 'pictales.post','uses' => 'PostPictalesController@getPictales',]);
Route::post('post/add', ['middleware' => ['auth'],'as' => 'pictales.add','uses' => 'PostPictalesController@postPictalesAdd',]);
Route::get('/post/{postId}/delete',['middleware' => ['auth'],'as' => 'pictales.delete','uses' => 'PostPictalesController@getPictalesdelete',]);
/**
*
* Likes and comments
*
**/
Route::post('post/{postId}/comments',['middleware' => ['auth'],'as' => 'comment_path','uses' => 'LikesAndCommentsController@postComment',]);
Route::get('post/{CommentId}/comments/delete',['middleware' => ['auth'],'as' => 'comment_delete_path','uses' => 'LikesAndCommentsController@getDeleteComment',]);
Route::get('post/{postId}/like',['middleware' => ['auth'],'as' => 'like_path','uses' => 'LikesAndCommentsController@getLike',]);
Route::get('post/{postId}/unlike',['middleware' => ['auth'],'as' => 'unlike_path','uses' => 'LikesAndCommentsController@getUnLike',]);
/**
*
* Share Links for Pictals Posts
*
**/
Route::get('/shared/{postName}',['as' => 'public_share_link','uses' => 'PostPictalesController@getShareLink',]);
/**
*
* Settings
*
**/
Route::get('/get/settings',['as' => 'settingss','uses' => 'SettingsController@main',]);


// Password reset link request routes...
Route::get('/password/email', 'Auth\PasswordController@getEmail');
Route::post('/password/email', 'Auth\PasswordController@postEmail');

// Password reset routes...
Route::get('/password/reset/{token}', 'Auth\PasswordController@getReset');
Route::post('/password/reset', 'Auth\PasswordController@postReset');


//Feedback page

Route::get('/upcoming/features/', ['middleware' => ['auth'],'as'=>'feedback', 'uses' => 'FeedbackController@main']);
Route::post('/upcoming/features/comment',['middleware' => ['auth'],'as' => 'suggestion_path','uses' => 'FeedbackController@postComment',]);
Route::get('/upcoming/features/comment',['middleware' => ['auth'],'uses' => 'FeedbackController@getComment',]);
Route::get('/upcoming/features/{feedbackId}/delete',['middleware' => ['auth'],'as' => 'suggestion_delete_path','uses' => 'FeedbackController@getDeleteComment',]);
Route::get('/upcoming/features/{feedbackId}/vote',['middleware' => ['auth'],'as' => 'vote_path','uses' => 'FeedbackController@getVote',]);
Route::get('/upcoming/features/{feedbackId}/unvote',['middleware' => ['auth'],'as' => 'unvote_path','uses' => 'FeedbackController@getUnvote',]);


//FAQ page

Route::get('info/FAQ', 'SettingsController@getFAQ');

/**
*
* Elastic Search
*
**/

Route::get('search/user', [
	'middleware' => 'ajax',
	'as' => 'search',
	'uses' => 'SearchController@search',
 ]);

Route::get('notify/readall', [
	'as' => 'notify.read',
	'uses' => 'NotifyController@readall',
 ]);


 Route::get('search/topic', [
 	'as' => 'topic.search',
 	'uses' => 'TopicController@Tsearch',
  ]);

	/*
	Admin
	*/
	Route::get('admin/home',['middleware' => ['auth'],'as' => 'admin', 'uses' => 'AdminController@view',]);
	Route::get('/{username}/{filename}',['as'=> 'pictales.name','uses' => 'PostPictalesController@getPictalesName',]);
