<?php

namespace App\Events;

Use App\Models\Like;
use App\Events\Event;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class SendWelcomeEmail extends Event
{
    use SerializesModels;
    public $like;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Like $like)
    {
          $this = like->$like;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
