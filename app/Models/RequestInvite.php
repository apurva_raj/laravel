<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RequestInvite extends Model
{
    protected $table = 'requests';

    protected $fillable = ['email', 'code','claimed_at', 'status'];

    protected $hidden = [];


    public function user() {
        return $this->belongsTo('App\Models\User');
    }

}
