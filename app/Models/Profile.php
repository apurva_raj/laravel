<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    protected $table = 'profiles';

    protected $fillable = ['display_pic', 'display_thumb_pic', 'status', 'bio', 'facebook_url', 'instagram_url', 'youtube_url','twitter_url'];

    protected $hidden = [];


    public function user() {
        return $this->belongsTo('App\Models\User');
    }

}
