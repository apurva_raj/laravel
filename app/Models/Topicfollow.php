<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Topicfollow extends Model {

	protected $table = 'topicfollow';

  protected $fillable = array('id','follower_id', 'topic_id',);


	public function user() {
		return $this->belongsToMany('App\Models\User', 'follower_id');
	}

}
