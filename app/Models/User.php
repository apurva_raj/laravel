<?php

namespace App\Models;

use App\Models\Post;
use Fenos\Notifynder\Notifable;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;
use Elasticquent\ElasticquentTrait;


class User extends Model implements AuthenticatableContract, CanResetPasswordContract, SluggableInterface
{
    use Authenticatable, CanResetPassword;
    use ElasticquentTrait;
    use Notifable;



    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'email', 'password', 'username'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];

    protected $mappingProperties = array(
        'name' => [
          'type' => "string",
          'analyzer' => "trigrams",
        ],
      );

    protected $analysis = array(
        'filter' => [
            'trigrams_filter' => [
                'type' => "ngram",
                'min_gram' => "3",
                'max_gram' => "3",
            ]
        ],
        'analyzer' => [
            'trigrams' => [
                'type' => 'custom',
                'tokenizer' => 'standard',
                'filter' =>   [
                    "lowercase",
                    "trigrams_filter"
                ]
            ]
        ]

    );

    use SluggableTrait;

    protected $sluggable = [
        'build_from' => 'name',
        'save_to'    => 'username',
    ];

    public function profile() {
        return $this->hasOne('App\Models\Profile');
    }

    public function post() {
        return $this->hasMany('App\Models\Post', 'user_id');
    }

     public function invite() {
        return $this->hasMany('App\Models\Invite', 'user_id');
    }
    public function feedback() {
        return $this->hasMany('App\Models\Feedback', 'user_id');
    }


    /**
     * Start User follow relationship
     */
    public function follow() {
        return $this->hasMany('App\Models\Follow', 'follower_id');
    }

    //user follows topic



    public function followstopic(){
      return $this->belongsToMany('App\Models\Topicfollow', 'topicfollow', 'follower_id', 'topic_id');
    }


    public function isfollow() {
        return $this->belongsToMany('App\Models\Topicfollow', 'follower_id');
    }

    public function myfollowers(){
      return $this->belongsToMany('App\Models\User', 'followers', 'followee_id', 'follower_id');
    }

    public function ImFollwing(){
      return $this->belongsToMany('App\Models\User', 'followers', 'follower_id', 'followee_id');
    }

    // public function followers() {
    //     return $this->belongsToMany('App\Models\User');
    // }
    /***  End User follow relationship
     */
     public function likes() {
       return $this->hasMany('App\Models\Like', 'user_id');
     }

     public function votes() {
       return $this->hasMany('App\Models\FeedbackVotes', 'user_id');
     }

      public function notify() {
       return $this->hasMany('Fenos\Notifynder\Models\Notification', 'from_id');
     }

     public function hasLiked(Post $post) {
       return (bool) $this->likes()->where('post_id',$post->id)->count();
     }

     public function isFollowing(User $user) {
         return (bool) $this->follow()->where('followee_id',$user->id)->count();
     }

     public function hasVoted(Feedback $feedback) {
       return (bool) $this->votes()->where('comment_id',$feedback->id)->count();
     }

     public function followt(){
       return $this->hasMany('App\Models\Topicfollow','follower_id');
     }


     public function isFollowingTopic(Topic $topic) {
         return (bool) $this->followt()->where('topic_id',$topic->id)->count();
     }

}
