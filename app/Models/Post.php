<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;

class Post extends Model implements  SluggableInterface
{
    protected $table = 'posts';

    protected $fillable = ['title', 'slug_title', 'post_pic', 'post_story', 'file_path', 'share_md5', 'stared'];

    protected $hidden = [];



    use SluggableTrait;

    protected $sluggable = [
        'build_from' => 'title',
        'save_to'    => 'slug_title',
    ];

    public function user() {
        return $this->belongsTo('App\Models\User','user_id');
    }
    public function likes() {
     return $this->hasMany('App\Models\Like', 'post_id');
    }


    // public function likes() {
    // 	return $this->morphMany('App\Models\Like', 'likes', 'id', 'post_id');
    // }

    public function comments() {
      return $this->hasMany('App\Models\Comment', 'post_id');
    }


    public function topics() {
        return $this->belongsToMany('App\Models\Topic');
    }



}
