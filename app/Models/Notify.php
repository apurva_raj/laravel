<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Notify extends Model {

	protected $fillable = ['from_id', 'to_id', 'category_id', 'url', 'extra', 't1', 't2'];


	protected $table = 'notifications';


	public function user() {
		return $this->belongsTo('App\Models\User', 'to_id');	}
	public function from_user(){
		return $this->belongsTo('App\Models\User', 'from_id');	}

}
