<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class surprise extends Model {

	protected $fillable = ['id', 'post_id'];


	protected $table = 'surprise';


	public function user() {
		return $this->belongsTo('App\Models\User', 'to_id');	}
	
}
