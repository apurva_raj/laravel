<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Feedback extends Model {

	protected $table = 'feedback';

  protected $fillable = ['id', 'user_id','comment'];

	public function user() {
		return $this->belongsTo('App\Models\User', 'user_id');
	}
	public function votes() {
	 return $this->hasMany('App\Models\FeedbackVotes', 'comment_id');
	}

}
