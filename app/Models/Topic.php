<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Topic extends Model {

	protected $table = 'topics';

  protected $fillable = ['id', 'topic_name', 'topic_cover_pic', 'topic_about'];

	public function post() {
		return $this->belongsToMany('App\Models\Post');
	}

	public function user() {
		return $this->belongsTo('App\Models\User','user_id');
	}

	public function topicfollowers(){
		return $this->belongsToMany('App\Models\User', 'topicfollow', 'topic_id', 'follower_id' );
	}



}
