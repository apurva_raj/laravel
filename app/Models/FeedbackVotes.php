<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FeedbackVotes extends Model {

	protected $table = 'feedback_votes';

  protected $fillable = ['id', 'user_id','comment_id'];

	public function user() {
		return $this->belongsTo('App\Models\Feedback', 'user_id');
	}

}
