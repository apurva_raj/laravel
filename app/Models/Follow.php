<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Follow extends Model {

	protected $table = 'followers';

    protected $fillable = array('follower_id', 'followee_id');


	public function user() {
		return $this->belongsTo('App\Models\User','follower_id');
	}


}
