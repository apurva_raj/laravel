<?php

use Illuminate\Database\Seeder;
use App\Models\User;
use App\Models\Profile;
use App\Models\Post;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        $limit = 100;

        for ($i = 0; $i < $limit; $i++) {
            $user=User::create([ //,
                'name' => $faker->name,
                'email' => $faker->unique()->email,
                'password' => bcrypt('trial'),
            ]);

            $profile = new Profile([
            	'display_pic' => $faker->imageUrl($width = 640, $height = 480),
            	'status' => $faker->sentence,
                'bio' => $faker->paragraph,
            ]);

            $user->profile()->save($profile);

            for ($p = 0; $p < $faker->numberBetween($min = 15, $max = 100); $p++) {
	            $post = new Post([
		           'file_path' => $faker->imageUrl($width = 640, $height = 480),
		           'post_story' =>$faker->paragraph,
		           'title' =>$faker->sentence,
	            ]);
	          	$user->post()->save($post);
	        }
        }
    }
}