<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\User;
use App\Models\Profile;

class CreateProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profiles', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->string('display_pic');
            $table->string('display_thumb_pic');
            $table->string('status');
            $table->text('bio');
            $table->string('facebook_url');
            $table->string('instagram_url');
            $table->string('youtube_url');
            $table->string('twitter_url');
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });

        $profile = new Profile([
           'display_pic' =>'/images/user-default.jpg',
           'display_thumb_pic' => '/images/user-default-thumb.jpg',
           'status' => 'Edit your status'
       ]);

       User::first()->profile()->save($profile);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('profiles');
    }
}
