<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTopicsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('topics', function (Blueprint $table) {
          $table->increments('id');
          $table->string('topic_name');
          $table->mediumText('topic_about');
          $table->string('topic_cover_pic');
          $table->string('topic_slug_name');
          $table->timestamps();

      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::drop('topics');
    }
}
