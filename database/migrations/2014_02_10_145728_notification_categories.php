<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class NotificationCategories extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('notification_categories', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->index();
            $table->string('text');
        });

        DB::table('notification_categories')->insert(
            array(
              'id' => '1',
              'name' => 'user.following',
              'text' => '{from.name} started following you',
            )
        );
        DB::table('notification_categories')->insert(
            array(
              'id' => '2',
              'name' => 'user.likes',
              'text' => '{from.name} commended your post {extra.title}',
            )
        );
        DB::table('notification_categories')->insert(
            array(
              'id' => '3',
              'name' => 'user.comments',
              'text' => '{from.name} replied to your post {extra.title}',
            )
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('notification_categories');
    }
}
