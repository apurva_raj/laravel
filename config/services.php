<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, Mandrill, and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => '',
        'secret' => '',
    ],

    'mandrill' => [
        'secret' => '',
    ],

    'ses' => [
        'key'    => '',
        'secret' => '',
        'region' => 'us-east-1',
    ],

    'stripe' => [
        'model'  => App\User::class,
        'key'    => '',
        'secret' => '',
    ],

   'facebook' => [
        'client_id' => '983885895016686',
        'client_secret' => '836654ee24cde08162343a2dac1abb3f',
        'redirect' => env('FACEBOOK_CALLBACK', 'Facebook'),
    ],

    'google' => [
        'client_id' => '649294644194-mlblocd4tap9us8gbta10s62fas91u6f.apps.googleusercontent.com',
        'client_secret' => 'G5H2Fwad-PJvxXKpU0dNXe1Q',
        'redirect' => env('GOOGLE_CALLBACK', 'Google'),
    ],

];
